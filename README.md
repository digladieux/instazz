# TP InstaZZ

## Étudiants

- Gonzales Florian
- Gladieux Cunha Dimitri

## Projet

### Thème 

Nous avons appellé notre projet **TrackTrip**. Le but est de faire un réseau social orienté photo de vacances. 
Les *globe trotters* sont les utilisateurs qui vont poster des publications. 
Les *tracks* (dont vous pouvez voir des parties de codes, mais qui n'ont pas pu être terminé) sont des lieux, villes, pays que nous suivons.

### Répartition 

Utilisation d'un git flow
1. featureDimitri & featureFlorian
2. Merge sur dev
3. Déploiement sur release
4. Merge sur master pour que vous puissez clone

## Technologies

- **Backend** : Nodejs - Express - TypeScript
  - **UML** : Modélisation de nos classes avec StarUML
- **Frontend** : Nodejs - React - TypeScript
  - **Figma** : Design de l'application - *https://www.figma.com/file/ERoUWtplO03ZVcPLAGOLn6/TrackTrip*
- **Hébergement** :
  - Frontend : Gitlab pages - *http://insta-zz.herokuapp.com/*
  - Backend : Google Cloud Platform (GCP) - *https://tracktrip-267316.appspot.com*
  - Images : Bucket sur GCP - *https://storage.googleapis.com/track-trip-pictures/NomImage*
  - Base de donnée : MongoLab - *mongodb+srv://digladieux:Adnadn_03@tracktrip-nl5xz.gcp.mongodb.net/track-trip?retryWrites=true&w=majority*
    - Collections : `globetrotters` et `posts`
- **CI/CD** : Publication du Backend via le fichier `.gitlab-ci.yml`
  - Déploiement du backend 
    - Image docker `google/cloud-sdk` sur la distribution linux `alpine`. 
    - Besoin des permissions pour accéder à GCP avec les variables d'environnements dans Gitlab (ou dans le fichier `keys.json` pour que vous les voyez)
    - `app.yml` correspond à un `.env` mais pour GCP
    - `.gcloudignore` correspond à un `.gitignore` mais pour GCP
  - Déploiement du frontend 
    - git subtree push --prefix frontend heroku master 

## Fonctionnalités Fonctionnelles

- Création d'un compte
- Mot de passe oublié (envoie d'un email avec redirection vers une adresse frontend)
- Confirmation d'adresse e-mail 
- Authentification via
  - Credentials (Identifiant et Mot de passe)
  - Cookie d'authorization contenant le token (qui provient d'une authentification par credentials précédente) 
- Création / Consultation / Consultation publication
- Création / Consultation (Il faut rafraichir la page..) compte utilisateur
- Création (Il faut rafraichir la page pour voir la modif) / Consultation commentaires
- Ajout de likes sur une personnes, publications, commentaires
- Recherche d'utilisateur
- Design Responsive : 3 Versions de l'application
  - Mobile
  - Tablette
  - Ordinateur
 
## Fonctionnalités Envisagées

Du à un manque de temps, nous n'avons pas pu faire toutes les fonctionnalités souhaitées. 
- Les tests unitaires marchent en local mais pas en CI ou beaucoup ne passe pas (car plus de 5sec pour la requete donc on a une erreur). Il doit falloir mettre une configuration mais pas pu eu le temps
- Les méthodes GET/POST/DELETE/PUT de globeTrotter, post et comment sont réalisé en backend mais non disponible sur le frontend
- Recovery password
- etc ..

## Remarques 

- Beaucoup de perte de temps pour le déploiement du frontend. Impossible sur une gitlab ou github pages. Nous avons du utilisé Heroku
- Le typescript nous a semblé une bonné idée au départ pour imposer les types, mais a été très contraignant car beaucoup de choses sont bloqués
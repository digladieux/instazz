declare module 'body-parser';
declare module 'express';
declare module 'dotenv';
declare module 'mongoose';
declare module 'guid-typescript';
declare module 'bcrypt';
declare module 'cors';
declare module 'jsonwebtoken';
declare module 'multer';
declare module 'nodemailer';
declare module 'supertest';




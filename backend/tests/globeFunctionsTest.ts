import request from 'supertest';
import app from '../src/index'

let idPost: string;

export const globeTrotterBouchon = {
    "trackers": [],
    "posts": [],
    "statistic_followers": [],
    "statistic_followed": [],
    "_id": "5e5e3844b5329a00115f3296",
    "username": "usernameTest",
    "password": "passwordTest",
    "email": "noreply.tracktrip@gmail.com",
    "profile_picture": "https://storage.googleapis.com/track-trip-pictures/picture-1583233091546.HPIM0675.JPG",
    "confirmation_email": false,
    "first_name": "firstNameTest",
    "last_name": "lastNameTest",
    "birth_date": "2020-01-01",
    "creation_date": "03/03/2020",
    "country_localisation": "FR",
    "biography": "biographyTest",
}

export async function getToken(): Promise<string> {

    const usernameTest = globeTrotterBouchon.username;
    const passwordTest = globeTrotterBouchon.password;
    const res = await request(app)
        .get(`/v1/authentication/loginByCredentials?username=${usernameTest}&password=${passwordTest}`)
        .set('Accept', 'application/json')
    return res.text;
}

export async function postPost(): Promise<any> {
    const token = await getToken();
    const res = await request(app)
        .post(`/v1/post`)
        .field('title', 'titleTest')
        .field('country', 'FR')
        .attach('picture', './tests/imageTest.jpg')
        .set('Authorization', token)
    expect(res.statusCode).toEqual(201);

    idPost = JSON.parse(res.text)._id;
}

export async function deletePost(): Promise<any> {
    const token = await getToken();
    const res = await request(app)
        .delete(`/v1/post/:?id=${idPost}`)
        .set('Authorization', token)
    expect(res.statusCode).toEqual(200);
}


export async function getPost(): Promise<any> {
    const token = await getToken();
    const res = await request(app)
        .get(`/v1/post/?id=${idPost}`)
        .set('Authorization', token);
    return JSON.parse(res.text);
}
import request from 'supertest';
import app from '../../src/index'
import { getToken, globeTrotterBouchon } from '../globeFunctionsTest';

const pathUrl = '/v1/post';

describe(`${pathUrl}/`, function () {

    const invalidData: string = 'toto';
    const titleTest: string = 'titleTest';
    let idPost: string;
    it('POST valid', async () => {
        const token = await getToken();
        const res = await request(app)
            .post(`${pathUrl}`)
            .field('title', titleTest)
            .field('country', 'FR')
            .attach('picture', './tests/imageTest.jpg')
            .set('Authorization', token)
        expect(res.statusCode).toEqual(201);

        idPost = JSON.parse(res.text)._id;
    })

    it('GET /:id valid', async () => {
        const token = await getToken();
        const res = await request(app)
            .get(`${pathUrl}/?id=${idPost}`)
            .set('Authorization', token)
        expect(res.statusCode).toEqual(200);
        expect(JSON.parse(res.text).title).toEqual(titleTest);
    })

    it('POST /postLike valid', async () => {
        const token = await getToken();
        let res = await request(app)
            .post(`${pathUrl}/postLike`)
            .send({ id_post: idPost })
            .set('Authorization', token)
        expect(res.statusCode).toEqual(201);

        res = await request(app)
            .get(`${pathUrl}/?id=${idPost}`)
            .set('Authorization', token)
        expect(res.statusCode).toEqual(200);

        const post = JSON.parse(res.text);
        expect(post.statistic[0]).toEqual(globeTrotterBouchon._id);

    })

    it('GET /:id invalid', async () => {
        const token = await getToken();
        const res = await request(app)
            .get(`${pathUrl}/?id=${invalidData}`)
            .set('Authorization', token)
        expect(res.statusCode).toEqual(400);
    })

    it('GET /me valid', async () => {
        const token = await getToken();
        const res = await request(app)
            .get(`${pathUrl}/me`)
            .set('Authorization', token)
        expect(res.statusCode).toEqual(200);
    })

    it('DELETE valid', async () => {
        const token = await getToken();
        const res = await request(app)
            .delete(`${pathUrl}/:?id=${idPost}`)
            .set('Authorization', token)
        expect(res.statusCode).toEqual(200);
    })

    it('DELETE invalid', async () => {
        const token = await getToken();
        const res = await request(app)
            .delete(`${pathUrl}/:?id=${invalidData}`)
            .set('Authorization', token)
        expect(res.statusCode).toEqual(400);
    })
});
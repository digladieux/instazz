import request from 'supertest';
import app from '../../src/index'
import { globeTrotterBouchon, getToken } from '../globeFunctionsTest';

const pathUrl = '/v1/globeTrotter';

describe(`${pathUrl}/`, function () {

    const invalidData: string = 'toto';
    it('GET /id valid', async () => {
        const idTest = globeTrotterBouchon._id;
        const token = await getToken();
        const res = await request(app)
            .get(`${pathUrl}/user/:?id=${idTest}`)
            .set('Accept', 'application/json')
            .set('Authorization', token)
        expect(res.statusCode).toEqual(200);
    })

    it('GET /id invalid', async () => {
        const token = await getToken();
        const res = await request(app)
            .get(`${pathUrl}/user/:?id=${invalidData}`)
            .set('Accept', 'application/json')
            .set('Authorization', token)
        expect(res.statusCode).toEqual(400);
    })

    it('GET /me valid', async () => {
        const token = await getToken();
        const res = await request(app)
            .get(`${pathUrl}/me`)
            .set('Accept', 'application/json')
            .set('Authorization', token)
        expect(res.statusCode).toEqual(200);
    })

    // it('PUT', async () => {
    //     const token = await getToken();

    //     let res = await request(app)
    //         .put(`${pathUrl}?username=${globeTrotterBouchon.username}&email=${globeTrotterBouchon.email}&first_name=toto&last_name=${globeTrotterBouchon.last_name}&country=${globeTrotterBouchon.country_localisation}&birth_date=${globeTrotterBouchon.birth_date}`)
    //         .set('Accept', 'application/json')
    //         .set('Authorization', token)
    //     expect(res.statusCode).toEqual(200);

    //     res = await request(app)
    //         .get(`${pathUrl}/me`)
    //         .set('Accept', 'application/json')
    //         .set('Authorization', token)
    //     expect(res.statusCode).toEqual(200);

    //     let globeTrotter = JSON.parse(res.text);
    //     expect(globeTrotter.first_name).toEqual('toto');

    //     res = await request(app)
    //         .put(`${pathUrl}?username=${globeTrotterBouchon.username}&email=${globeTrotterBouchon.email}&first_name=${globeTrotterBouchon.first_name}&last_name=${globeTrotterBouchon.last_name}&country=${globeTrotterBouchon.country_localisation}&birth_date=${globeTrotterBouchon.birth_date}`)
    //         .set('Accept', 'application/json')
    //         .set('Authorization', token)
    //     expect(res.statusCode).toEqual(200);

    //     res = await request(app)
    //         .get(`${pathUrl}/me`)
    //         .set('Accept', 'application/json')
    //         .set('Authorization', token)
    //     expect(res.statusCode).toEqual(200);

    //     globeTrotter = JSON.parse(res.text);
    //     expect(globeTrotter.first_name).toEqual(globeTrotterBouchon.first_name);

    // })
});
import request from 'supertest';
import app from '../../src/index'
import { getToken, postPost, getPost, deletePost } from '../globeFunctionsTest';

const pathUrl = '/v1/comment/';

describe(`${pathUrl}`, function () {

    it('POST valid', async () => {

        await postPost();
        let post = await getPost();

        const token = await getToken();
        let res = await request(app)
            .post(`${pathUrl}`)
            .send({ id_post: post._id, description: 'descriptionTest' })
            .set('Accept', 'application/json')
            .set('Authorization', token)
        expect(res.statusCode).toEqual(201);

        post = await getPost();
        expect(post.comments[0].description).toEqual('descriptionTest');

    })

    // it('DELETE valid', async () => {

    //     let post = await getPost();

    //     const token = await getToken();
    //     let res = await request(app)
    //         .delete(`${pathUrl}:?id_post=${post._id}&id_comment=${post.comments[0]._id}`)
    //         .set('Accept', 'application/json')
    //         .set('Authorization', token);
    //     expect(res.statusCode).toEqual(200);

    //     post = await getPost();
    //     expect(post.comments.length).toEqual(0);
    //     await deletePost();
    // })
});
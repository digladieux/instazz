import request from 'supertest';
import app from '../../src/index'
import { globeTrotterBouchon, getToken } from '../globeFunctionsTest';

const pathUrl = '/v1/authentication/';

describe(`${pathUrl}`, function () {

    const invalidData: string = 'toto';
    it('loginByCredentials valid', async () => {
        const usernameTest = globeTrotterBouchon.username;
        const passwordTest = globeTrotterBouchon.password;
        const res = await request(app)
            .get(`${pathUrl}loginByCredentials?username=${usernameTest}&password=${passwordTest}`)
            .set('Accept', 'application/json')
        expect(res.statusCode).toEqual(200);
    })

    it('loginByCredentials invalid username', function (done) {
        request(app)
            .get(`${pathUrl}loginByCredentials?username=${invalidData}&password=${invalidData}`)
            .set('Accept', 'application/json')
            .expect(400, done);
    });

    it('loginByCredentials invalid password', function (done) {
        const usernameTest = globeTrotterBouchon.username;
        request(app)
            .get(`${pathUrl}loginByCredentials?username=${usernameTest}&password=${invalidData}`)
            .set('Accept', 'application/json')
            .expect(400, done);
    });

    it('loginByToken valid', async function (done) {
        const token = await getToken();
        request(app)
            .get(`${pathUrl}loginByToken?token=${token}`)
            .set('Accept', 'application/json')
            .expect(200, done);
    });

    it('loginByToken invalid', function (done) {
        request(app)
            .get(`${pathUrl}loginByToken?token=${invalidData}`)
            .set('Accept', 'application/json')
            .expect(400, done);
    });

});
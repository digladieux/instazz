import winston from 'winston';
import ILogger from './ILogger';

let formatConsoleLog: any = winston.format.combine(
    winston.format.colorize({
        all: true
    }),
    winston.format.label({
        label: '[LOGGER]'
    }),
    winston.format.timestamp({
        format: "YY-MM-DD HH:MM:SS"
    }),
    winston.format.printf(
        log => ` ${log.label}  ${log.timestamp} - ${log.level} : ${log.message}`
    )
);

var debug: winston.Logger = winston.createLogger({
    levels: {
        debug: 0
    },
    transports: [
        new (winston.transports.File)({ filename: 'logs/debug.log', level: 'debug' }),
        new (winston.transports.Console)({
            level: 'debug',
            format: winston.format.combine(winston.format.colorize(), formatConsoleLog)
        })
    ]
});

var info: winston.Logger = winston.createLogger({
    levels: {
        info: 1
    },
    transports: [
        new (winston.transports.File)({ filename: 'logs/info.log', level: 'info' }),
        new (winston.transports.Console)({
            level: 'info',
            format: winston.format.combine(winston.format.colorize(), formatConsoleLog)
        })
    ]
});

var warn: winston.Logger = winston.createLogger({
    levels: {
        warn: 2
    },
    transports: [
        new (winston.transports.File)({ filename: 'logs/warn.log', level: 'warn' }),
        new (winston.transports.Console)({
            level: 'warn',
            format: winston.format.combine(winston.format.colorize(), formatConsoleLog)
        })

    ]
});


var error: winston.Logger = winston.createLogger({
    levels: {
        error: 3
    },
    transports: [
        new (winston.transports.File)({ filename: 'logs/error.log', level: 'error' }),
        new (winston.transports.Console)({
            level: 'error',
            format: winston.format.combine(winston.format.colorize(), formatConsoleLog)
        })
    ]
});

var logger: ILogger = {
    debug: function (msg: any): void {
        debug.debug(msg);
    },
    info: function (msg: any): void {
        info.info(msg);
    },
    warn: function (msg: any): void {
        warn.warn(msg);
    },
    error: function (msg: any): void {
        error.error(msg);
    },
};

export default logger;

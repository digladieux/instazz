import globe_trotter_route from './routes/globeTrotterRoute'
import post_route from './routes/postRoute'
import authentication_route from './routes/authenticationRoute'
import express from "express";
import cors from 'cors';
import LoggerMiddleware from './middlewares/loggerMiddleware'
import comment_route from './routes/commentRoute';
import research_route from './routes/researchRoute';
import bodyParser from 'body-parser';

const app = express()

app.use(cors());
app.options('*', cors())
app.use(bodyParser.json());
app.use(LoggerMiddleware);

app.use('/v1/globeTrotter', globe_trotter_route);
app.use('/v1/post', post_route);
app.use('/v1/comment', comment_route);
app.use('/v1/authentication', authentication_route);
app.use('/v1/research', research_route);
export default app;
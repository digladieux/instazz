import express from 'express';
import bodyParser from 'body-parser';
import GlobeTrotterController from '../controllers/globeTrotterController';
import AuthorizationMiddleware from '../middlewares/authorizationMiddleware';

const globe_trotter_controller: GlobeTrotterController = new GlobeTrotterController();

let globe_trotter_route = express.Router();

globe_trotter_route.use((req: Request, res: Response, next: any) => { AuthorizationMiddleware(req, res, next) })

globe_trotter_route.use(bodyParser.json());
globe_trotter_route.use(bodyParser.urlencoded({ extended: false }));

globe_trotter_route.get("/user/:id", (req: Request, res: Response) => {
    globe_trotter_controller.getGlobeTrotterById(req, res);
});
globe_trotter_route.get("/me", (req: Request, res: Response) => {
    globe_trotter_controller.getMe(req, res);
});

globe_trotter_route.post("/postLike", (req: Request, res: Response) => {
    globe_trotter_controller.postLike(req, res);
});

globe_trotter_route.put("/", (req: Request, res: Response) => {
    globe_trotter_controller.putGlobeTrotter(req, res);
});


globe_trotter_route.delete("/:id", (req: Request, res: Response) => { globe_trotter_controller.delete(req, res) });

export default globe_trotter_route;




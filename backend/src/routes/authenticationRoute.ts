import express from 'express';
import bodyParser from 'body-parser';
import AuthenticationController from '../controllers/authenticationController';
import { uploader, savePictureGoogleCloudPlatform } from '../middlewares/storagePicture';
import logger from '../winstonLogger';

const authentication_controller: AuthenticationController = new AuthenticationController();

const authentication_route = express.Router();

authentication_route.use(bodyParser.json());
authentication_route.use(bodyParser.urlencoded({ extended: false }));

authentication_route.get("/loginByCredentials", (req: Request, res: Response) => {
    authentication_controller.loginByCredentials(req, res);
});

authentication_route.get("/loginByToken", (req: Request, res: Response) => {
    authentication_controller.loginByToken(req, res);
});
authentication_route.get("/resetPassword", (req: Request, res: Response) => {
    authentication_controller.resetPassword(req, res);
});
authentication_route.post("/confirmationEmail", (req: Request, res: Response) => {
    authentication_controller.confirmationEmail(req, res);
});
authentication_route.post('/globeTrotter', function (req: any, res: any, next: any) {
    uploader(req, res, function (err: any) {
        if (err) {
            res.status(400).send(err.message);
        }
        const picture_name: string = savePictureGoogleCloudPlatform(req, res, next);
        return authentication_controller.postGlobeTrotter(req, res, picture_name)
    })
})


authentication_route.get("/resetPassword", (req: Request, res: Response) => {
    authentication_controller.resetPassword(req, res);
});

export default authentication_route;




import express from 'express';
import bodyParser from 'body-parser';
import AuthorizationMiddleware from '../middlewares/authorizationMiddleware';
import CommentController from '../controllers/commentController';
import logger from '../winstonLogger';

const comment_controller: CommentController = new CommentController();

let comment_route = express.Router();

comment_route.use((req: Request, res: Response, next: any) => { AuthorizationMiddleware(req, res, next) })

comment_route.use(bodyParser.json());
comment_route.use(bodyParser.urlencoded({ extended: false }));

comment_route.delete("/:id", (req: Request, res: Response) => { comment_controller.delete(req, res) });
comment_route.post("/", (req: Request, res: Response) => {
    comment_controller.post(req, res)
});

export default comment_route;




import express from 'express';
import bodyParser from 'body-parser';
import ResearchController from '../controllers/researchController';

const research_controller: ResearchController = new ResearchController();

const research_route = express.Router();

research_route.use(bodyParser.json());
research_route.use(bodyParser.urlencoded({ extended: false }));

research_route.get("/", (req: Request, res: Response) => {
    research_controller.getResearch(req, res);
});

export default research_route;




import express from 'express';
import bodyParser from 'body-parser';
import PostController from '../controllers/postController';
import { uploader, savePictureGoogleCloudPlatform } from '../middlewares/storagePicture';
import AuthorizationMiddleware from '../middlewares/authorizationMiddleware';
import logger from '../winstonLogger';

let post_controller: PostController = new PostController();
let route_post = express.Router();

route_post.use(bodyParser.json());
route_post.use(bodyParser.urlencoded({ extended: false }));
route_post.use((req: Request, res: Response, next: any) => { AuthorizationMiddleware(req, res, next) })

route_post.get("/", (req: Request, res: Response) => { post_controller.getById(req, res) });
route_post.get("/globeTrotter", (req: Request, res: Response) => { post_controller.getPostsGlobeTrotter(req, res) });
route_post.get("/me", (req: Request, res: Response) => { post_controller.getMyPost(req, res) });
route_post.get("/history", (req: Request, res: Response) => { post_controller.getBySize(req, res) });
route_post.delete("/:id", (req: Request, res: Response) => { post_controller.delete(req, res) });
route_post.post('/', function (req: any, res: any, next: any) {
    uploader(req, res, function (err: any) {
        if (err) {
            logger.error(err.message)
            res.status(400).send(err.message);
        }
        const picture_name: string = savePictureGoogleCloudPlatform(req, res, next);
        return post_controller.post(req, res, picture_name)
    })
})

route_post.post("/postLike", (req: Request, res: Response) => {
    post_controller.postLike(req, res);
});
export default route_post;


import ITrackerBusiness from "../interfaces/ITrackersBusiness";
import ITrackerDatabase from "../../database/interfaces/ITrackerDatabase";
import TrackerCollectionMongoose from "../../database/mongo/trackerCollectionMongoose";
import { ID_NOT_FOUND } from "../../controllers/Messages/errors";
class TrackerBusiness implements ITrackerBusiness {

    private trackerDatabase: ITrackerDatabase = new TrackerCollectionMongoose();
    async getById(id: string): Promise<any | null> {
        return await this.trackerDatabase.getById(id);
    }

    async getByResearch(research: string): Promise<any[] | null> {
        return await this.trackerDatabase.getTrackersByQuery(
            { value: new RegExp(research, 'i') }
        )
    }

    async getByValue(value: string): Promise<any | null> {
        return await this.trackerDatabase.getTrackerByQuery({ value: value })
    }

    async postTrackers(trackers: string[], id_globe_trotter: string): Promise<void> {

        trackers.forEach(async tracker => {
            const result = await this.getByValue(tracker);
            if (result) {
                await this.trackerDatabase.postTracker(tracker);
            } else {
                result.statistic.push(id_globe_trotter);
                await this.putTracker(result);
            }
        });
    }

    async postLike(id_globe_trotter: string, id_tracker: string): Promise<any | string> {
        var result = await this.getById(id_tracker);

        if (!result) {
            return ID_NOT_FOUND;
        }

        result.statistic.push(id_globe_trotter);
        return await this.putTracker(result);

    }
    async putTracker(data: any): Promise<void> {
        return await this.trackerDatabase.putTracker(data);
    }
}

export default TrackerBusiness;
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import ITokenAuthentication from '../interfaces/ITokenAuthenticationBusiness';
import logger from '../../winstonLogger';

class TokenAuthenticationBusiness implements ITokenAuthentication {
    private _private_key: string;

    constructor() {
        dotenv.config();
        if (process.env.PRIVATE_KEY) {
            this._private_key = process.env.PRIVATE_KEY
        } else {
            logger.error("PRIVATE_KEY not find");
            throw "PRIVATE_KEY not find";
        }
    }

    encodeToken(id: string): string {
        return jwt.sign({
            id: id
        }, this._private_key, { expiresIn: 60 * 60 * 60 * 60 });
    }

    getIdFromToken(token: string | null): string | null {

        try {
            var decoded = jwt.verify(token, this._private_key);
            return decoded.id;
        } catch (err) {
            logger.error("Token Invalid : " + token);
            return null;
        }
    }

    isTokenValid(token: string | null): Boolean {

        try {
            var decoded = jwt.verify(token, this._private_key);
            return true;
        } catch (err) {
            logger.error("Token Invalid : " + token);
            return false;
        }
    }
}

export default TokenAuthenticationBusiness;
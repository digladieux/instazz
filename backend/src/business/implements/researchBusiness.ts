import IGlobeTrotterBusiness from "../interfaces/IGlobeTrotterBusiness";
import GlobeTrotterBusiness from "./globeTrotterBusiness";
import ITrackerBusiness from "../interfaces/ITrackersBusiness";
import TrackerBusiness from "./trackerBusiness";

class ResearchBusiness {

    private globeTrotterBusiness: IGlobeTrotterBusiness = new GlobeTrotterBusiness()
    private trackerBusiness: ITrackerBusiness = new TrackerBusiness()

    async getResearch(research: string): Promise<any> {
        return {
            globe_trotter: await this.globeTrotterBusiness.getGlobeTrotterByResearch(research),
            trackers: await this.trackerBusiness.getByResearch(research)
        }
    }
}

export default ResearchBusiness;
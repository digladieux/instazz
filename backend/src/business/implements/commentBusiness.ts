import logger from "../../winstonLogger";
import { INVALID_ARGUMENT, ID_NOT_FOUND, DELETE_FORBIDDEN } from "../../controllers/Messages/errors";
import { getDateNow } from "../globalFunction";
import ICommentBusiness from "../interfaces/ICommentBusiness";
import IPostBusiness from "../interfaces/IPostBusiness";
import PostBusiness from "./postBusiness";

class CommentBusiness implements ICommentBusiness {

    private postBusiness: IPostBusiness = new PostBusiness();

    async deleteComment(id_post: string, id_comment: string, id_globe_trotter: string): Promise<void | string> {

        var post: any | null = await this.postBusiness.getPostById(id_post);
        if (!post) {
            return ID_NOT_FOUND
        }

        const comment: any | null = post.comments.find((comment: any) => comment._id === id_comment);
        if (!comment) {
            return ID_NOT_FOUND;
        }

        if (comment.id_globe_trotter !== id_globe_trotter) {
            return DELETE_FORBIDDEN;
        }

        post.comments = post.comments.filter((comment: any) => comment._id === id_comment);
        await this.postBusiness.putPost(post)
    }

    async postComment(data: any, id: string): Promise<any | string> {
        if (!data.description || !data.id_post) {
            return INVALID_ARGUMENT;
        }

        const result = await this.postBusiness.getPostById(data.id_post);
        if (!result) {
            return ID_NOT_FOUND;
        }
        const comment = {
            description: data.description,
            statistic: [],
            id_globe_trotter: id,
            publication_date: getDateNow(),
        }

        result.comments.push(comment)
        await this.postBusiness.putPost(result);
        return result;
    }

    async postLike(id_globe_trotter: string, id_post: string, id_comment: string): Promise<void | string> {
        var post = await this.postBusiness.getPostById(id_post);

        if (!post) {
            return ID_NOT_FOUND;
        }

        const comment = post.comments.filter((comment: any) => comment._id === id_comment);

        if (!comment) {
            return ID_NOT_FOUND;
        }

        let isPresent = false;
        if (comment.statistic !== undefined && comment.statistic.length !== 0) {
            comment.statistic.forEach((id: string) => {
                if (id === id_globe_trotter) {
                    isPresent = true;
                }
            });
        } else {
            comment.statistic = []
        }


        if (isPresent) {
            comment.statistic = comment.statistic.filter((id: any) => id !== id_globe_trotter);
        } else {
            comment.statistic.push(id_globe_trotter);
        }
        post.comment.map((comment: any) => post.comment.find((com: any) => com._id === comment._id) || comment);

        await this.postBusiness.putPost(post);
        return comment;

    }

    async putComment(id_post: string, id_comment: string, description: string): Promise<void | string> {
        var post = await this.postBusiness.getPostById(id_post);
        if (!post) {
            return ID_NOT_FOUND;
        }

        const comment = post.comments.filter((comment: any) => comment._id === id_comment);

        if (!comment) {
            return ID_NOT_FOUND;
        }
        comment.description = description;
        post.comment.map((comment: any) => post.comment.find((com: any) => com._id === comment._id) || comment);
        await this.postBusiness.putPost(post);
        return comment;
    }
}

export default CommentBusiness;
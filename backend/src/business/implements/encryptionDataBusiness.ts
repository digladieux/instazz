import bcrypt from 'bcrypt';

const SALT_ROUND = 10;

class EncryptionDataBusiness implements IEncryptionDataBusiness {

    hashData(data: string): string {
        return bcrypt.hashSync(data, bcrypt.genSaltSync(SALT_ROUND));
    }

    compare(data: string, data_hashed: string) {
        return bcrypt.compareSync(data, data_hashed);
    }

}

export default EncryptionDataBusiness;
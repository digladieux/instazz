import IAuthenticationBusiness from '../interfaces/IAuthenticationBusiness';
import IGlobeTrotterBusiness from '../interfaces/IGlobeTrotterBusiness';
import GlobeTrotterBusiness from './globeTrotterBusiness';
import ITokenAuthenticationBusiness from '../interfaces/ITokenAuthenticationBusiness';
import TokenAuthenticationBusiness from './tokenAuthenticationBusiness';
import ISendEmailBusiness from '../interfaces/ISendEmailBusiness';
import SendEmailNodeMailerBusiness from './sendEmailNodemailerBusiness';
import EncryptionDataBusiness from "../../business/implements/encryptionDataBusiness";
import { USERNAME_NOT_FOUND, INVALID_CREDENTIALS, INVALID_TOKEN, EMAIL_UNCONFIRMED } from '../../controllers/Messages/errors';
import logger from '../../winstonLogger';
class AuthenticationBusiness implements IAuthenticationBusiness {

    private globeTrotterBusiness: IGlobeTrotterBusiness = new GlobeTrotterBusiness();
    private encryptionData: IEncryptionDataBusiness = new EncryptionDataBusiness();
    private tokenAuthentication: ITokenAuthenticationBusiness = new TokenAuthenticationBusiness();
    private sendEmailBusiness: ISendEmailBusiness = new SendEmailNodeMailerBusiness();

    async loginByCredentials(username: string, password: string): Promise<string> {

        const globeTrotter = await this.globeTrotterBusiness.getGlobeTrotterByUsername(username);
        if (!globeTrotter) {
            return USERNAME_NOT_FOUND;
        }

        if (!globeTrotter.confirmation_email) {
            return EMAIL_UNCONFIRMED;
        }

        if (!this.encryptionData.compare(password, globeTrotter.hash_password)) {
            return INVALID_CREDENTIALS;
        }

        return this.tokenAuthentication.encodeToken(globeTrotter._id);

    }

    loginByToken(token: string): Boolean {
        return this.tokenAuthentication.isTokenValid(token);
    }

    async confirmationEmail(token: string): Promise<void | string> {

        const id: string | null = this.tokenAuthentication.getIdFromToken(token);
        if (id === null) {
            return INVALID_TOKEN;
        }

        return await this.globeTrotterBusiness.confirmationEmailGlobeTrotter(id);
    }

    async resetPassword(email: string): Promise<void | string> {

        const result: any | null = await this.globeTrotterBusiness.getGlobeTrotterByEmail(email);
        if (result === null) {
            return USERNAME_NOT_FOUND;

        }
        const token = this.tokenAuthentication.encodeToken(result._id)
        this.sendEmailBusiness.resetPasswordEmail(result.email, result.first_name + " " + result.last_name, token);
    }

    async postGlobeTrotter(data: any, profile_picture: string): Promise<any | string> {

        return await this.globeTrotterBusiness.postGlobeTrotter(data, profile_picture);
    };

}

export default AuthenticationBusiness;

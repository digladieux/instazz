import logger from "../../winstonLogger";
import ISendEmail from "../../business/interfaces/ISendEmailBusiness";
import SendEmailNodeMailer from "../../business/implements/sendEmailNodemailerBusiness";
import ITokenAuthentication from "../interfaces/ITokenAuthenticationBusiness";
import TokenAuthentication from "../../business/implements/tokenAuthenticationBusiness";
import { INVALID_ARGUMENT, USERNAME_NOT_FOUND, INVALID_ID, EMAIL_ALREADY_USED, USERNAME_ALREADY_USED, USER_NOT_FOUND, ID_NOT_FOUND, INVALID_CREDENTIALS } from "../../controllers/Messages/errors";
import IGlobeTrotterDatabase from "../../database/interfaces/IGlobeTrotterDatabase";
import GlobeTrotterCollectionMongoose from "../../database/mongo/globeTrotterCollectionMongoose";
import IGlobeTrotterBusiness from "../interfaces/IGlobeTrotterBusiness";
class GlobeTrotterBusiness implements IGlobeTrotterBusiness {

    private globeTrotterDatabase: IGlobeTrotterDatabase = new GlobeTrotterCollectionMongoose();
    private sendEmail: ISendEmail = new SendEmailNodeMailer();
    private generateToken: ITokenAuthentication = new TokenAuthentication();

    async getGlobeTrotterById(id: string): Promise<any | null> {
        return await this.globeTrotterDatabase.getGlobeTrotterById(id);
    }

    async getGlobeTrotterByEmail(email: string): Promise<any | null> {
        return await this.globeTrotterDatabase.getOneGlobeTrotterByQuery({ email: email });
    }

    async getGlobeTrotterByUsername(username: string): Promise<any | null> {
        return await this.globeTrotterDatabase.getOneGlobeTrotterByQuery({ username: username });
    }

    async getGlobeTrotterByResearch(research: string): Promise<any | null> {
        var query = {
            $or: [
                { first_name: new RegExp(research, 'i') },
                { last_name: new RegExp(research, 'i') },
                { username: new RegExp(research, 'i') },
            ]
        }

        return await this.globeTrotterDatabase.getGlobeTrottersByQuery(query);
    }



    async postLike(id_globe_trotter: string, id_globe_trotter_received_like: string): Promise<void | string> {
        var result = await this.getGlobeTrotterById(id_globe_trotter_received_like);

        if (!result) {
            return USER_NOT_FOUND;
        }

        let isPresent = false;
        if (result.statistic_followed !== undefined && result.statistic_followed.length !== 0) {
            result.statistic_followed.forEach((id: string) => {
                if (id === id_globe_trotter) {
                    isPresent = true;
                }
            });
        } else {
            result.statistic_followed = []
        }

        if (isPresent) {
            result.statistic_followed = result.statistic_followed.filter((id: any) => id !== id_globe_trotter);
        } else {
            result.statistic_followed.push(id_globe_trotter);
        }

        await this.globeTrotterDatabase.putGlobeTrotter(result);

        result = await this.getGlobeTrotterById(id_globe_trotter);

        if (!result) {
            return USER_NOT_FOUND;
        }

        isPresent = false;
        if (result.statistic_followers !== undefined && result.statistic_followers.length !== 0) {
            result.statistic_followers.forEach((id: string) => {
                if (id === id_globe_trotter_received_like) {
                    isPresent = true;
                }
            });
        } else {
            result.statistic_followers = []
        }

        if (isPresent) {
            result.statistic_followers = result.statistic_followers.filter((id: any) => id !== id_globe_trotter_received_like);
        } else {
            result.statistic_followers.push(id_globe_trotter_received_like);
        }
        await this.globeTrotterDatabase.putGlobeTrotter(result);
    }

    async putGlobeTrotter(data: any): Promise<any | string> {
        return await this.globeTrotterDatabase.putGlobeTrotter(data);
    }

    async putAccountInfo(data: any, id: string): Promise<any | string> {
        var result = await this.getGlobeTrotterById(id);

        if (!result) {
            return USER_NOT_FOUND;
        }

        result.username = data.username;
        result.email = data.email;
        result.first_name = data.first_name;
        result.last_name = data.last_name;
        result.birth_date = data.birth_date;
        result.country_localisation = data.country;
        result.biography = data.biography;
        return await this.globeTrotterDatabase.putGlobeTrotter(result);
    }

    async deleteGlobeTrotter(id: string): Promise<any | null> {
        return await this.globeTrotterDatabase.deleteGlobeTrotter(id);
    }

    async confirmationEmailGlobeTrotter(id: string): Promise<void | string> {

        const result = await this.getGlobeTrotterById(id);

        if (!result) {
            logger.error(ID_NOT_FOUND + ' : ' + id);
            return ID_NOT_FOUND + ' : ' + id;
        }

        result.confirmation_email = true;
        this.putGlobeTrotter(result);
    }

    async postGlobeTrotter(data: any, profile_picture: string): Promise<any | string> {

        if (!data.username || !data.password || !data.email
            || !data.first_name || !data.last_name || !data.birth_date
            || !data.country) {
            logger.error(INVALID_ARGUMENT);
            return INVALID_ARGUMENT;
        }

        const userByEmail = await this.getGlobeTrotterByEmail(data.email);
        if (userByEmail !== null && userByEmail.length !== 0) {
            logger.error(EMAIL_ALREADY_USED + data.email);
            return EMAIL_ALREADY_USED + data.email;
        }
        const userByUsername = await this.getGlobeTrotterByUsername(data.username);
        if (userByUsername !== null && userByUsername.length !== 0) {
            logger.error(USERNAME_ALREADY_USED + data.username);
            return USERNAME_ALREADY_USED + data.username;
        }
        const result: any | string = await this.globeTrotterDatabase.postGlobeTrotter(data, profile_picture);

        if (typeof result === 'string') {
            return result;
        }

        const token = this.generateToken.encodeToken(result._id);
        this.sendEmail.confirmationEmail(data.email, data.first_name + " " + data.last_name, token);
        return result;

    }
}

export default GlobeTrotterBusiness;
import logger from "../../winstonLogger";
import nodemailer from 'nodemailer'
import IEmailTemplate from "../emailTemplate/IEmailTemplate";
import ISendEmail from "../interfaces/ISendEmailBusiness";
import confirmationEmailTemplate from "../emailTemplate/confirmationEmail";
import dotenv from 'dotenv';
import resetPasswordEmail from "../emailTemplate/resetPasswordEmail";



class SendEmailNodeMailerBusiness implements ISendEmail {

  private CALLBACK_URL: string;
  private EMAIL_HOST: string;
  private EMAIL_PORT: number;
  private EMAIL_ADDRESS: string;
  private EMAIL_PASSWORD: string;

  constructor() {
    dotenv.config();
    if (process.env.FRONT_END_URL && process.env.EMAIL_ADDRESS
      && process.env.EMAIL_PASSWORD && process.env.EMAIL_PORT
      && process.env.EMAIL_HOST) {
      this.CALLBACK_URL = process.env.FRONT_END_URL
      this.EMAIL_HOST = process.env.EMAIL_HOST;
      this.EMAIL_PORT = parseInt(process.env.EMAIL_PORT);
      this.EMAIL_ADDRESS = process.env.EMAIL_ADDRESS;
      this.EMAIL_PASSWORD = process.env.EMAIL_PASSWORD;
    } else {
      logger.error("ENV VARIABLE not find");
      throw "ENV VARIABLE not find";
    }
  }

  async confirmationEmail(destination: string, globeTrotter: string, token: string): Promise<void> {
    var emailTemplate: IEmailTemplate = confirmationEmailTemplate;
    emailTemplate.plain_text = emailTemplate.plain_text.replace("USER", globeTrotter)
    emailTemplate.plain_text = emailTemplate.plain_text.replace("TOKEN", this.CALLBACK_URL + "callback?context=confirmedEmail&token=" + token)
    emailTemplate.html = emailTemplate.html.replace("USER", globeTrotter)
    emailTemplate.html = emailTemplate.html.replace("TOKEN", this.CALLBACK_URL + "callback?context=confirmedEmail&token=" + token)
    await this.sendEmail([destination], emailTemplate);
  }

  async resetPasswordEmail(destination: string, globeTrotter: string, token: string): Promise<void> {
    var emailTemplate: IEmailTemplate = resetPasswordEmail;
    emailTemplate.plain_text = emailTemplate.plain_text.replace("USER", globeTrotter)
    emailTemplate.plain_text = emailTemplate.plain_text.replace("TOKEN", this.CALLBACK_URL + "callback?context=resetPassword&token=" + token)
    emailTemplate.html = emailTemplate.html.replace("USER", globeTrotter)
    emailTemplate.html = emailTemplate.html.replace("TOKEN", this.CALLBACK_URL + "callback?context=resetPassword&token=" + token)
    await this.sendEmail([destination], emailTemplate);
  }


  private createTransport() {
    return nodemailer.createTransport({
      host: this.EMAIL_HOST,
      port: this.EMAIL_PORT,
      secure: true,
      requireTLS: true,
      auth: {
        user: this.EMAIL_ADDRESS,
        pass: this.EMAIL_PASSWORD

      }
    });
  }
  private getMailOptions(destinations: string, email: IEmailTemplate) {
    return {
      from: this.EMAIL_ADDRESS,
      to: destinations,
      subject: email.subject,
      text: email.plain_text,
      html: email.html,
      attachments: email.attachments
    };
  }

  private async sendEmail(destinations: string[], email: IEmailTemplate): Promise<void> {

    if (destinations.length === 0 || !email.plain_text || !email.html) {
      logger.error("Invalid Argument for email");
    }

    else {

      const destinations_to_string = destinations.toString();
      let transporter = this.createTransport();
      var mailOptions = this.getMailOptions(destinations_to_string, email);
      await transporter.sendMail(mailOptions, function (error: any, info: any) {
        if (error) {
          logger.error(error);
        } else {
          logger.info('Email sent : ' + info.response);
        }
      });
    }

  }
}

export default SendEmailNodeMailerBusiness;
import mongoose from "mongoose";
import { INVALID_ARGUMENT, ID_NOT_FOUND, USER_NOT_FOUND, INVALID_ID } from "../../controllers/Messages/errors";
import IPostDatabase from "../../database/interfaces/IPostDatabase";
import PostCollectionMongoose from "../../database/mongo/postCollectionMongoose";
import IPostBusiness from "../interfaces/IPostBusiness";
import GlobeTrotterBusiness from "./globeTrotterBusiness";
import IGlobeTrotterBusiness from "../interfaces/IGlobeTrotterBusiness";
import logger from "../../winstonLogger";
class PostBusiness implements IPostBusiness {

    private postDatabase: IPostDatabase = new PostCollectionMongoose();
    private globeTrotterBusiness: IGlobeTrotterBusiness = new GlobeTrotterBusiness();
    async getPostById(id: string): Promise<any | null> {
        return await this.postDatabase.getPostById(id);
    }

    async getPostsBySize(page_size: string, page_number: string): Promise<any | string> {
        const page_number_int = parseInt(page_number);
        const page_size_int = parseInt(page_size);
        if (page_number_int < 0 || page_size_int < 0 || await this.postDatabase.getCountPost() < page_size_int * page_number_int) {
            return INVALID_ARGUMENT;
        }

        const posts = await this.postDatabase.getPostsBySize(page_size_int, page_number_int);
        const globeTrotters = new Array<any>();
        if (typeof posts === 'string') {
            return posts;
        } else {
            await Promise.all(posts.map(async (post) => {
                const globeTrotter = await this.globeTrotterBusiness.getGlobeTrotterById(post.id_globe_trotter);
                globeTrotters.push(globeTrotter);
            }));
            return {
                posts: posts,
                globe_trotters: globeTrotters
            }
        }
    }

    async getPostsGlobeTrotter(id: string): Promise<any[] | string> {
        return this.getMyPosts(id);
    }
    async getMyPosts(id: string): Promise<any[] | string> {
        const result: any | null = await this.globeTrotterBusiness.getGlobeTrotterById(id);
        if (!result) {
            return USER_NOT_FOUND;
        }

        var posts = new Array();
        await Promise.all(result.posts.map(async (id_post: string) => {
            const post = await this.getPostById(id_post);
            posts.push(post);
        }));

        return posts;

    }
    async deletePost(id: string): Promise<any | string> {

        const post = await this.getPostById(id);

        if (!post) {
            return INVALID_ID;
        }

        let globeTrotter = await this.globeTrotterBusiness.getGlobeTrotterById(post.id_globe_trotter);

        if (!globeTrotter) {
            return INVALID_ID;
        }

        globeTrotter.posts = globeTrotter.posts.filter((post: any) => post._id === id);
        await this.globeTrotterBusiness.putGlobeTrotter(globeTrotter);
        return await this.postDatabase.deletePost(id);
    }


    async postPost(data: any, id_globe_trotter: string, picture_name: string): Promise<string | any> {

        if (!data.title || !data.country) {
            return INVALID_ARGUMENT;
        }

        return await this.postDatabase.postPost(data, id_globe_trotter, picture_name);
    }

    async postLike(id_globe_trotter: string, id_post: string): Promise<any | string> {
        var result = await this.getPostById(id_post);
        if (!result) {
            return ID_NOT_FOUND;
        }

        let isPresent = false;
        if (result.statistic !== undefined && result.statistic.length !== 0) {
            result.statistic.forEach((id: string) => {
                if (id === id_globe_trotter) {
                    isPresent = true;
                }
            });
        } else {
            result.statistic = []
        }

        if (isPresent) {
            result.statistic = result.statistic.filter((id: any) => id !== id_globe_trotter);
        } else {
            result.statistic.push(id_globe_trotter);
        }

        await this.putPost(result);
        return result.statistic;

    }
    async putPost(data: any): Promise<void> {
        return await this.postDatabase.putPost(data);
    }
}

export default PostBusiness;
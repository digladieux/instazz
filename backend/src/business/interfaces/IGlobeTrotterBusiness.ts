interface IGlobeTrotterBusiness {

    getGlobeTrotterById(id: string): Promise<any | null>;
    getGlobeTrotterByEmail(email: string): Promise<any | null>;
    getGlobeTrotterByUsername(username: string): Promise<any | null>;
    getGlobeTrotterByResearch(research: string): Promise<any | null>;

    postLike(id_globe_trotter: string, id_globe_trotter_received_like: string): Promise<void | string>;

    putAccountInfo(data: any, id: string): Promise<any | string>;
    putGlobeTrotter(data: any): Promise<any | null>;

    deleteGlobeTrotter(id: string): Promise<any | null>;

    confirmationEmailGlobeTrotter(id: string): Promise<void | string>;

    postGlobeTrotter(data: any, profile_picture: string): Promise<any | string>;

}

export default IGlobeTrotterBusiness;
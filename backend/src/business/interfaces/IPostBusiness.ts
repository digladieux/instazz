interface IPostBusiness {

    getPostById(id: string): Promise<any | null>;
    getPostsBySize(page_size: string, page_number: string): Promise<any[] | string>;
    deletePost(id: string): Promise<any | null>;
    postPost(data: any, id_globe_trotter: string, picture_name: string): Promise<string | any>;
    getMyPosts(id: string): Promise<any[] | string>;
    postLike(id_globe_trotter: string, id_post: string): Promise<any | string>;
    putPost(data: any): Promise<void>;
    getPostsGlobeTrotter(id: string): Promise<any[] | string>;
}

export default IPostBusiness;
interface ITokenAuthenticationBusiness {
    encodeToken(id: string): string
    getIdFromToken(token: string): string | null;
    isTokenValid(token: string): Boolean;
}

export default ITokenAuthenticationBusiness;
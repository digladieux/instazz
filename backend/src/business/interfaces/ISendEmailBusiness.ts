interface ISendEmailBusiness {
    confirmationEmail(destination: string, globeTrotter: string, token: string): Promise<void>;
    resetPasswordEmail(destination: string, globeTrotter: string, token: string): Promise<void>;
}

export default ISendEmailBusiness;
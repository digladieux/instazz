interface ITrackerBusiness {

    getById(id: string): Promise<any | null>;
    getByResearch(research: string): Promise<any[] | null>;
    getByValue(value: string): Promise<any | null>;

    postTrackers(trackers: string[], id_globe_trotter: string): Promise<void>;

    postLike(id_globe_trotter: string, id_tracker: string): Promise<any | string>;
    putTracker(data: any): Promise<void>;
}

export default ITrackerBusiness;
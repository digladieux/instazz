interface IAuthenticationBusiness {

    loginByCredentials(username: string, password: string): Promise<string>;
    loginByToken(token: string): Boolean;
    confirmationEmail(token: string): Promise<void | string>;
    resetPassword(email: string): Promise<void | string>;
    postGlobeTrotter(data: any, profile_picture: string): Promise<any | string>;
}

export default IAuthenticationBusiness;

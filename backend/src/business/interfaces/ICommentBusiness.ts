interface ICommentBusiness {

    deleteComment(id_post: string, id_comment: string, id_globe_trotter: string): Promise<void | string>;
    postComment(data: any, id: string): Promise<string | void>;
    postLike(id_globe_trotter: string, id_post: string, id_comment: string): Promise<void | string>;
    putComment(id_post: string, id_comment: string, description: string): Promise<void | string>;
}

export default ICommentBusiness;
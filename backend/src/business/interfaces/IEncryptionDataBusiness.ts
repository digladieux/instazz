interface IEncryptionDataBusiness {
    hashData(data: string): string;
    compare(data: string, data_hashed: string): boolean;
}
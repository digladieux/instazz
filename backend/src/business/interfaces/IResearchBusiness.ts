import GlobeTrotterCollection from "../../database/mongo/globeTrotterCollectionMongoose";

interface IResearchBusiness {

    getResearch(research: string): Promise<any>;
}

export default IResearchBusiness;
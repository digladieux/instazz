import IEmailTemplate from "./IEmailTemplate";

const confirmationEmailTemplate: IEmailTemplate = {
    subject: "Account created - TrackTrip",
    plain_text:
        "Dear USER, " +
        "Thank for creation an account in TrackTrip" +
        "Please click on this link to confirm you email address" +
        "TOKEN" +
        "TrackTrip Team",
    html:
        'Dear USER, <br>' +
        '<img src="cid:favicon_track_trip" alt="favicon" /><br>' +
        '<h1>Thank for creation an account in TrackTrip</h1><br>' +
        'Please click on this link to confirm you email address<br>' +
        'TOKEN' + '<br>' +
        'TrackTrip Team',
    attachments: [{
        filename: 'icon_track_trip_480x480.png',
        path: 'upload/icons/icon_track_trip_480x480.png',
        cid: 'favicon_track_trip'
    }]
}

export default confirmationEmailTemplate;
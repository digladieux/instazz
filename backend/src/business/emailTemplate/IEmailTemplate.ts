type IEmailTemplate = {
    subject: string,
    plain_text: string,
    html: string,
    attachments: {
        filename: string,
        path: string,
        cid: string
    }[]
}

export default IEmailTemplate;
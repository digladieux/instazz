import IEmailTemplate from "./IEmailTemplate";

const resetPasswordEmail: IEmailTemplate = {
    subject: "Reset Password - TrackTrip",
    plain_text:
        "Dear USER, " +
        "You forget password ? Please use this link to create a new password " +
        "TOKEN" +
        "If you don't ask to change your password, please contact our team" +
        "TrackTrip Team",
    html:
        'Dear USER, <br>' +
        '<img src="cid:favicon_track_trip" alt="favicon" /><br>' +
        "You forget password ? Please use this link to create a new password " +
        "TOKEN" +
        "If you don't ask to change your password, please contact our team" +
        "TrackTrip Team",
    attachments: [{
        filename: 'icon_track_trip_480x480.png',
        path: 'upload/icons/icon_track_trip_480x480.png',
        cid: 'favicon_track_trip'
    }]
}

export default resetPasswordEmail;
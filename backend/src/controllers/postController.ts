import PostCollection from '../database/mongo/postCollectionMongoose';
import { TITLE_REQUIRED, DELETE_FORBIDDEN, COUNTRY_REQUIRED, PICTURE_REQUIRED, ID_REQUIRED, ID_NOT_FOUND, PAGE_NUMBER_REQUIRED, PAGE_SIZE_REQUIRED } from './Messages/errors';
import logger from '../winstonLogger';
import IPostBusiness from '../business/interfaces/IPostBusiness';
import PostBusiness from '../business/implements/postBusiness';

class PostController {

    private postBusiness: IPostBusiness = new PostBusiness();
    async getById(req: any, res: any) {
        if (!req.query.id) {
            return res.status(400).send(ID_REQUIRED);
        }

        const result: any | null = await this.postBusiness.getPostById(req.query.id);
        if (!result) {
            return res.status(400).send(ID_NOT_FOUND);
        }
        return res.status(200).send(result);
    }

    async getPostsGlobeTrotter(req: any, res: any) {
        if (!req.query.id) {
            return res.status(400).send(ID_REQUIRED);
        }
        const result: any[] | string = await this.postBusiness.getPostsGlobeTrotter(req.query.id);

        if (typeof result === 'string') {
            return res.status(400).send(ID_NOT_FOUND);
        }
        return res.status(200).send(result);
    }
    async getMyPost(req: any, res: any) {

        const result: any[] | string = await this.postBusiness.getMyPosts(req.local.id);

        if (typeof result === 'string') {
            return res.status(400).send(ID_NOT_FOUND);
        }
        return res.status(200).send(result);
    }

    async getBySize(req: any, res: any): Promise<void> {
        if (!req.query.page_number) {
            return res.status(400).send(PAGE_NUMBER_REQUIRED);
        } else if (!req.query.page_size) {
            return res.status(400).send(PAGE_SIZE_REQUIRED);
        }

        const result: any | string = await this.postBusiness.getPostsBySize(req.query.page_size, req.query.page_number);
        if (typeof result === 'string') {
            return res.status(400).send(result);
        }
        return res.status(200).send(result);
    }

    async postLike(req: any, res: any): Promise<void> {

        if (!req.body.id_post) {
            return res.status(400).send(ID_REQUIRED);
        }

        const result: any | string = await this.postBusiness.postLike(req.local.id, req.body.id_post);
        if (typeof result === 'string') {
            return res.status(400).send(result);
        }
        return res.status(201).send(result);
    }

    async post(req: any, res: any, picture_name: string) {
        if (!req.body.title) {
            return res.status(400).send(TITLE_REQUIRED);
        } else if (!req.body.country) {
            return res.status(400).send(COUNTRY_REQUIRED);
        }

        const result: string | any = await this.postBusiness.postPost(req.body, req.local.id, picture_name);
        if (typeof result === 'string') {
            return res.status(400).send(result);
        }
        return res.status(201).send(result);
    }

    async delete(req: any, res: any) {
        if (!req.query.id) {
            return res.status(400).send(ID_REQUIRED);
        }
        const post: any | null = await this.postBusiness.getPostById(req.query.id);
        if (!post) {
            return res.status(400).send(ID_NOT_FOUND);
        }

        if (post.id_globe_trotter !== req.local.id) {
            return res.status(403).send(DELETE_FORBIDDEN);
        }

        const result: any | string = await this.postBusiness.deletePost(req.query.id);

        if (typeof result === 'string') {
            return res.status(400).send(result);
        }
        return res.status(200).send(result);
    }
}

export default PostController;


import { LAST_NAME_REQUIRED, INVALID_TOKEN, FIRST_NAME_REQUIRED, BIRTH_DATE_REQUIRED, COUNTRY_REQUIRED, USERNAME_REQUIRED, PASSWORD_REQUIRED, EMAIL_REQUIRED, INVALID_CREDENTIALS, USER_NOT_FOUND, TOKEN_REQUIRED } from './Messages/errors';
import { ID_REQUIRED, ID_NOT_FOUND } from './Messages/errors';
import IGlobeTrotterBusiness from '../business/interfaces/IGlobeTrotterBusiness';
import GlobeTrotterBusiness from '../business/implements/globeTrotterBusiness';
class GlobeTrotterController {

    private globeTrotterBusiness: IGlobeTrotterBusiness = new GlobeTrotterBusiness();

    async getGlobeTrotterById(req: any, res: any): Promise<void> {

        if (!req.query.id) {
            return res.status(400).send(ID_REQUIRED);
        }

        const result: any | null = await this.globeTrotterBusiness.getGlobeTrotterById(req.query.id);
        if (!result) {
            return res.status(400).send(result);
        }
        return res.status(200).send(result);
    }

    async getMe(req: any, res: any): Promise<void> {

        const result: any | null = await this.globeTrotterBusiness.getGlobeTrotterById(req.local.id);
        if (typeof result === 'string') {
            return res.status(400).send(result);
        }
        return res.status(200).send(result);
    }

    async postLike(req: any, res: any): Promise<void> {
        if (!req.body.id_globe_trotter) {
            return res.status(400).send(ID_REQUIRED);
        }
        const result: void | string = await this.globeTrotterBusiness.postLike(req.local.id, req.body.id_globe_trotter);
        if (typeof result === 'string') {
            return res.status(400).send(result);
        }
        return res.status(201).send(result);
    }

    async delete(req: any, res: any): Promise<void> {
        if (!req.query.id) {
            return res.status(400).send(ID_REQUIRED);
        }

        const result: any | null = await this.globeTrotterBusiness.deleteGlobeTrotter(req.query.id);
        if (result) {
            return res.status(200).send(result);
        }
        return res.status(401).send(ID_NOT_FOUND);
    }

    async putGlobeTrotter(req: any, res: any): Promise<void> {
        if (!req.query.username) {
            return res.status(400).send(USERNAME_REQUIRED);
        } else if (!req.query.email) {
            return res.status(400).send(EMAIL_REQUIRED);
        } else if (!req.query.first_name) {
            return res.status(400).send(FIRST_NAME_REQUIRED);
        } else if (!req.query.last_name) {
            return res.status(400).send(LAST_NAME_REQUIRED);
        } else if (!req.query.country) {
            return res.status(400).send(COUNTRY_REQUIRED);
        } else if (!req.query.birth_date) {
            return res.status(400).send(BIRTH_DATE_REQUIRED);
        }

        const globeTrotter: any | string = await this.globeTrotterBusiness.putAccountInfo(req.query, req.local.id);
        if (typeof globeTrotter === 'string') {
            return res.status(400).send(globeTrotter);
        }
        return res.status(200).send(globeTrotter);
    };


}

export default GlobeTrotterController;

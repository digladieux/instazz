import { RESEARCH_REQUIRED } from "./Messages/errors";
import ResearchBusiness from "../business/implements/researchBusiness";
import IResearchBusiness from "../business/interfaces/IResearchBusiness";

class ResearchController {

    private researchBusiness: IResearchBusiness = new ResearchBusiness();

    async getResearch(req: any, res: any): Promise<void> {
        if (!req.query.research) {
            return res.status(400).send(RESEARCH_REQUIRED);
        }

        let result: any | string = await this.researchBusiness.getResearch(req.query.research);
        if (typeof result === 'string') {
            return res.status(400).send(result);
        }
        return res.status(200).send(result);
    }
}

export default ResearchController;

import { ID_NOT_FOUND, ID_COMMENT_REQUIRED, ID_POST_REQUIRED, DESCRIPTION_REQUIRED } from './Messages/errors';
import logger from '../winstonLogger';
import CommentBusiness from '../business/implements/commentBusiness';
import ICommentBusiness from '../business/interfaces/ICommentBusiness';

class CommentController {

    private commentBusiness: ICommentBusiness = new CommentBusiness();
    async post(req: any, res: any) {
        if (!req.body.id_post) {
            return res.status(400).send(ID_POST_REQUIRED);
        } else if (!req.body.description) {
            return res.status(400).send(DESCRIPTION_REQUIRED);
        }

        const result = await this.commentBusiness.postComment(req.body, req.local.id);
        if (typeof result === 'string') {
            return res.status(400).send(result);
        }

        return res.status(201).send(result);
    }

    async postLike(req: any, res: any): Promise<void> {
        if (!req.body.id_post) {
            return res.status(400).send(ID_POST_REQUIRED);
        } else if (!req.body.id_comment) {
            return res.status(400).send(ID_POST_REQUIRED);
        }

        const result: any | null = await this.commentBusiness.postLike(req.local.id, req.query.id_post, req.query.id_comment);
        if (!result) {
            return res.status(201).send(result);
        }
        return res.status(400).send(result);
    }

    async delete(req: any, res: any) {

        logger.debug(req.query)
        if (!req.query.id_comment) {
            return res.status(400).send(ID_COMMENT_REQUIRED);
        } else if (!req.query.id_post) {
            return res.status(400).send(ID_POST_REQUIRED);
        }

        const result: any | string = await this.commentBusiness.deleteComment(req.query.id_post, req.query.id_comment, req.local.id);

        if (typeof result === 'string') {
            return res.status(400).send(ID_NOT_FOUND);
        }
        return res.status(200).send(result);
    }

    async put(req: any, res: any) {
        if (!req.query.id_comment) {
            return res.status(400).send(ID_COMMENT_REQUIRED);
        } else if (!req.query.id_post) {
            return res.status(400).send(ID_POST_REQUIRED);
        } else if (!req.query.description) {
            return res.status(400).send(DESCRIPTION_REQUIRED);
        }

        const result: any | null = await this.commentBusiness.putComment(req.query.id_post, req.query.id_comment, req.query.description);

        return res.status(200).send("Not Implemented");
    }
}

export default CommentController;


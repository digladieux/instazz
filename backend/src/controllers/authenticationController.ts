import { LAST_NAME_REQUIRED, INVALID_TOKEN, FIRST_NAME_REQUIRED, BIRTH_DATE_REQUIRED, COUNTRY_REQUIRED, USERNAME_REQUIRED, PASSWORD_REQUIRED, EMAIL_REQUIRED, INVALID_CREDENTIALS, USER_NOT_FOUND, TOKEN_REQUIRED, USERNAME_NOT_FOUND, EMAIL_UNCONFIRMED } from './Messages/errors';
import { EMAIL_CONFIRMED, EMAIL_SENDED, VALID_TOKEN } from './Messages/success';
import IAuthenticationBusiness from '../business/interfaces/IAuthenticationBusiness';
import AuthenticationBusiness from '../business/implements/authenticationBusiness';
class AuthenticationController {

    private authenticationBusiness: IAuthenticationBusiness = new AuthenticationBusiness();
    async loginByCredentials(req: any, res: any): Promise<void> {
        if (!req.query.username) {
            return res.status(400).send(USERNAME_REQUIRED);
        } else if (!req.query.password) {
            return res.status(400).send(PASSWORD_REQUIRED);
        }

        let result: string = await this.authenticationBusiness.loginByCredentials(req.query.username, req.query.password);
        if (result === USERNAME_NOT_FOUND || result === EMAIL_UNCONFIRMED || result === INVALID_CREDENTIALS) {
            return res.status(400).send(result);
        }
        return res.status(200).send(result);
    }

    async loginByToken(req: any, res: any): Promise<void> {
        if (!req.query.token) {
            return res.status(400).send(TOKEN_REQUIRED);
        }
        if (!this.authenticationBusiness.loginByToken(req.query.token)) {
            return res.status(400).send(INVALID_TOKEN);
        }
        return res.status(200).send(VALID_TOKEN);

    }

    async confirmationEmail(req: any, res: any): Promise<void> {
        if (!req.body.token) {
            return res.status(400).send(TOKEN_REQUIRED);
        }

        const result: void | string = await this.authenticationBusiness.confirmationEmail(req.body.token);
        if (typeof result === 'string') {
            return res.status(400).send(result);
        }
        return res.status(200).send(EMAIL_CONFIRMED);

    }

    async resetPassword(req: any, res: any): Promise<void> {
        if (!req.query.email) {
            return res.status(400).send(EMAIL_REQUIRED);
        }

        const result: void | string = await this.authenticationBusiness.resetPassword(req.query.email);
        if (typeof result === 'string') {
            return res.status(400).send(USER_NOT_FOUND);

        }
        return res.status(200).send(EMAIL_SENDED);
    }

    async postGlobeTrotter(req: any, res: any, picture_name: string): Promise<void> {
        if (!req.body.username) {
            return res.status(400).send(USERNAME_REQUIRED);
        } else if (!req.body.password) {
            return res.status(400).send(PASSWORD_REQUIRED);
        } else if (!req.body.email) {
            return res.status(400).send(EMAIL_REQUIRED);
        } else if (!req.body.first_name) {
            return res.status(400).send(FIRST_NAME_REQUIRED);
        } else if (!req.body.last_name) {
            return res.status(400).send(LAST_NAME_REQUIRED);
        } else if (!req.body.country) {
            return res.status(400).send(COUNTRY_REQUIRED);
        } else if (!req.body.birth_date) {
            return res.status(400).send(BIRTH_DATE_REQUIRED);
        }

        const globeTrotter: any | string = await this.authenticationBusiness.postGlobeTrotter(req.body, picture_name);
        if (typeof globeTrotter === 'string') {
            return res.status(400).send(globeTrotter);
        }
        return res.status(201).send(globeTrotter);
    };

}

export default AuthenticationController;

export const ID_REQUIRED: string = "id is required";
export const USERNAME_REQUIRED: string = "username is required";
export const PASSWORD_REQUIRED: string = "password is required";
export const TOKEN_REQUIRED: string = "token is required";
export const EMAIL_REQUIRED: string = "email is required";
export const FIRST_NAME_REQUIRED: string = "first_name is required";
export const LAST_NAME_REQUIRED: string = "last_name is required";
export const COUNTRY_REQUIRED: string = "country is required";
export const BIRTH_DATE_REQUIRED: string = "birth_date is required";
export const TITLE_REQUIRED: string = "title is required";
export const PICTURE_REQUIRED: string = "picture is required";
export const ID_COMMENT_REQUIRED: string = "id_comment is required"
export const ID_POST_REQUIRED: string = "id_post is required"
export const DESCRIPTION_REQUIRED: string = "description is required";
export const PAGE_SIZE_REQUIRED: string = "page_size is required";
export const PAGE_NUMBER_REQUIRED: string = "page_number is required";
export const RESEARCH_REQUIRED: string = "research is required";

export const INVALID_TOKEN: string = "Invalid token";
export const INVALID_ARGUMENT: string = "Invalid argument";
export const INVALID_CREDENTIALS: string = "Invalid Credentials";
export const INVALID_ID: string = "Invalid Id";

export const ID_NOT_FOUND: string = "Id not found inside database";
export const USER_NOT_FOUND: string = "User not found";
export const USERNAME_NOT_FOUND: string = "username not found";

export const DELETE_FORBIDDEN: string = "No permission to delete";

export const EMAIL_UNCONFIRMED: string = "You need to confirm your email";
export const EMAIL_ALREADY_USED: string = "Email already used : ";
export const USERNAME_ALREADY_USED: string = "Username already used : ";

export const BACK_END_URL_NOT_FIND: string = "BACK_END_URL not find";

import mongoose from 'mongoose';

const TrackerSchema = new mongoose.Schema({
    value: { type: [String], required: true },
    statistic: { type: [String], required: true },
});

export default TrackerSchema; 
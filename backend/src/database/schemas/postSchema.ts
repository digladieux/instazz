import mongoose from 'mongoose';

const PostSchema = new mongoose.Schema({
    id_globe_trotter: { type: String },
    title: { type: String, required: true },
    description: String,
    publication_date: { type: String, required: true },
    shooting_date: String,
    pictures: { type: [String], required: true },
    country: { type: String, required: true },
    location: String,
    comments: [{
        description: { type: String, required: true },
        statistic: { type: [String], required: true },
        id_globe_trotter: { type: String },
        publication_date: { type: String, required: true },
    }],
    hashtags: [String],
    statistic: { type: [String], required: true },
});

export default PostSchema; 
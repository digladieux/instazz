import mongoose from 'mongoose';
const GlobeTrotterSchema = new mongoose.Schema({
    username: { type: String, required: true },
    hash_password: { type: String, required: true },
    email: { type: String, required: true },
    profile_picture: String,
    confirmation_email: { type: Boolean, required: true },
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    birth_date: { type: String, required: true },
    creation_date: { type: String, required: true },
    country_localisation: { type: String },
    biography: String,
    trackers: [String],
    posts: [String],
    statistic_followers: { type: [String], required: true },
    statistic_followed: { type: [String], required: true },
});

export default GlobeTrotterSchema;
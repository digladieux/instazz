interface IGlobeTrotterDatabase {

    getGlobeTrotterById(id: string): Promise<any | null>;
    getOneGlobeTrotterByQuery(query: {}): Promise<any | null>;
    getGlobeTrottersByQuery(query: {}): Promise<any[] | null>;

    putGlobeTrotter(data: any): Promise<any | null>;

    deleteGlobeTrotter(id: string): Promise<any | null>;

    postGlobeTrotter(data: any, profile_picture: string): Promise<any | string>;

}

export default IGlobeTrotterDatabase;
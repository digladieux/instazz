interface IPostDatabase {

    getPostById(id: string): Promise<any | null>;
    getPostsBySize(page_size: number, page_number: number): Promise<any[] | string>;
    getCountPost(): Promise<number>;
    deletePost(id: string): Promise<any | null>;
    postPost(data: any, id_globe_trotter: string, picture_name: string): Promise<string | any>;

    putPost(data: any): Promise<void>;
}

export default IPostDatabase;
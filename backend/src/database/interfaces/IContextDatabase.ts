import mongoose from "mongoose";
import dotenv from "dotenv";

interface IContextDatabase {

    isIdValid(id: string): boolean;

    connection(): void;

    disconnect(): Promise<void>;
}

export default IContextDatabase;
interface ITrackerDatabase {

    getById(id: string): Promise<any | null>;
    getTrackersByQuery(query: {}): Promise<any[] | null>;
    getTrackerByQuery(query: {}): Promise<any | null>;

    postTracker(tracker: string): Promise<void>;

    putTracker(data: any): Promise<void>;
}

export default ITrackerDatabase;
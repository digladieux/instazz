import mongoose from "mongoose";
import ContextDatabaseMongoose from './contextMongooseDatabase';
import PostSchema from '../schemas/postSchema';
import logger from "../../winstonLogger";
import dotenv from "dotenv";
import { ID_NOT_FOUND, INVALID_ARGUMENT } from "../../controllers/Messages/errors";
import GlobeTrotterCollection from "./globeTrotterCollectionMongoose";
import { getDateNow } from "../../business/globalFunction";
import IPostDatabase from "../interfaces/IPostDatabase";
import IContextDatabase from "../interfaces/IContextDatabase";
class PostCollectionMongoose implements IPostDatabase {
    private globeTrotterCollection: GlobeTrotterCollection = new GlobeTrotterCollection();
    private postModel = mongoose.model('Post', PostSchema);
    private contextDatabase: IContextDatabase = new ContextDatabaseMongoose();

    async getPostById(id: string): Promise<any | null> {

        if (!this.contextDatabase.isIdValid(id)) {
            return null;
        }
        this.contextDatabase.connection();
        const result: any | null = await this.postModel.findById(id);
        await this.contextDatabase.disconnect();
        return result;

    }

    async getCountPost(): Promise<number> {
        this.contextDatabase.connection();
        const result: number = await this.postModel.countDocuments();
        await this.contextDatabase.disconnect();
        return result;

    }
    async getPostsBySize(page_size: number, page_number: number): Promise<any[] | string> {
        if (page_number < 0 || page_size < 0 || await this.getCountPost() < page_size * page_number) {
            return INVALID_ARGUMENT;
        }

        this.contextDatabase.connection();
        const result: any[] = await this.postModel.find().sort({ publication_date: -1 }).skip(page_number * page_size).limit(page_size);
        await this.contextDatabase.disconnect();
        return result;

    }

    async deletePost(id: string): Promise<any | null> {
        if (!this.contextDatabase.isIdValid(id)) {
            return null;
        }
        this.contextDatabase.connection();

        const result: any | null = await this.postModel.findOneAndDelete({ _id: id });
        await this.contextDatabase.disconnect();
        return result;

    }


    async postPost(data: any, id_globe_trotter: string, picture_name: string): Promise<string | any> {

        const result = await this.globeTrotterCollection.getGlobeTrotterById(id_globe_trotter);
        if (!result) {
            return ID_NOT_FOUND;
        }

        var post = new this.postModel({
            id_globe_trotter: id_globe_trotter,
            title: data?.title,
            description: data?.description,
            publication_date: getDateNow(),
            shooting_date: data?.shooting_date,
            pictures: picture_name,
            country: data?.country[0],
            location: data?.location,
            hashtags: data?.hashtags,
            statistic: []
        });


        this.contextDatabase.connection();

        await post.save();
        result.posts.push(post._id);
        await this.contextDatabase.disconnect();
        await this.globeTrotterCollection.putGlobeTrotter(result);
        return post;


    }

    async putPost(data: any): Promise<void> {
        this.contextDatabase.connection();
        await this.postModel.replaceOne({ _id: data._id }, data);
        await this.contextDatabase.disconnect();

    }
}

export default PostCollectionMongoose;
import mongoose from "mongoose";
import GlobeTrotterSchema from "../schemas/globeTrotterSchema";
import ContextDatabaseMongoose from './contextMongooseDatabase';
import logger from "../../winstonLogger";
import dotenv from 'dotenv';
import { getDateNow } from "../../business/globalFunction";
import TrackersCollection from "./trackerCollectionMongoose";
import EncryptionDataBusiness from "../../business/implements/encryptionDataBusiness";
import IGlobeTrotterDatabase from "../interfaces/IGlobeTrotterDatabase";
import IContextDatabase from "../interfaces/IContextDatabase";
import { BACK_END_URL_NOT_FIND } from "../../controllers/Messages/errors";
class GlobeTrotterCollectionMongoose implements IGlobeTrotterDatabase {

    private encryptionData: IEncryptionDataBusiness = new EncryptionDataBusiness();
    private trackerDatabase: TrackersCollection = new TrackersCollection();
    private globeTrotterModel = mongoose.model('globeTrotter', GlobeTrotterSchema);
    private contextDatabase: IContextDatabase = new ContextDatabaseMongoose();

    async getGlobeTrotterById(id: string): Promise<any | null> {

        if (!this.contextDatabase.isIdValid(id)) {
            return null;
        }
        this.contextDatabase.connection();
        const result: any | null = await this.globeTrotterModel.findById(id);
        await this.contextDatabase.disconnect();
        return result;
    }

    async getOneGlobeTrotterByQuery(query: {}): Promise<any | null> {
        this.contextDatabase.connection();
        const result: any | null = await this.globeTrotterModel.findOne(query);
        await this.contextDatabase.disconnect();
        return result;

    }

    async getGlobeTrottersByQuery(query: {}): Promise<any[] | null> {
        this.contextDatabase.connection();
        const result: any[] | null = await this.globeTrotterModel.find(query);
        await this.contextDatabase.disconnect();
        return result;
    }


    async putGlobeTrotter(data: any): Promise<any | null> {
        this.contextDatabase.connection();
        const result: any | null = await this.globeTrotterModel.replaceOne({ _id: data._id }, data);
        await this.contextDatabase.disconnect();
        return result;
    }

    async deleteGlobeTrotter(id: string): Promise<any | null> {
        if (!this.contextDatabase.isIdValid(id)) {
            return null;
        }
        this.contextDatabase.connection();
        const result: any | null = await this.globeTrotterModel.findOneAndDelete({ _id: id });
        await this.contextDatabase.disconnect();
        return result;
    }

    async postGlobeTrotter(data: any, profile_picture: string): Promise<any | string> {
        var hash_password = this.encryptionData.hashData(data?.password);

        const globeTrotter = new this.globeTrotterModel({
            username: data.username,
            hash_password: hash_password,
            email: data.email,
            profile_picture: profile_picture,
            confirmation_email: false,
            first_name: data.first_name,
            last_name: data.last_name,
            birth_date: data.birth_date,
            creation_date: getDateNow(),
            country_localisation: data.country,
            biography: data?.biography,
            trackers: data?.trackers,
            statistic_followers: [],
            statistic_followed: []
        });
        this.contextDatabase.connection();
        await globeTrotter.save();
        if (data.trackers) {
            data.trackers.forEach(async (tracker: string) => {
                const result = await this.trackerDatabase.getTrackerByQuery({ value: tracker });
                if (result) {
                    await this.trackerDatabase.postTracker(tracker);
                } else {
                    result.statistic.push(globeTrotter._id);
                    await this.trackerDatabase.putTracker(result);
                }
            });
        }

        await this.contextDatabase.disconnect();
        return globeTrotter;

    }
}

export default GlobeTrotterCollectionMongoose;
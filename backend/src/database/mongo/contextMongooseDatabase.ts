import mongoose from "mongoose";
import dotenv from "dotenv";
import IContextDatabase from "../interfaces/IContextDatabase";

class ContextDatabaseMongoose implements IContextDatabase {

    isIdValid(id: string): boolean {
        return mongoose.Types.ObjectId.isValid(id);
    }

    connection() {
        dotenv.config();
        mongoose.connect(
            process.env.MONGO,
            {
                useNewUrlParser: true,
                useUnifiedTopology: false
            }
            , function (err: any) {
                if (err) throw err;
            });
    }

    async disconnect(): Promise<void> {
        await mongoose.disconnect();
    }


}

export default ContextDatabaseMongoose;
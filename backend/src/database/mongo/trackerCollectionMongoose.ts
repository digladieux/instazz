import mongoose from "mongoose";
import ContextDatabaseMongoose from './contextMongooseDatabase';
import TrackerSchema from "../schemas/trackerSchema";
import ITrackerDatabase from "../interfaces/ITrackerDatabase";
import IContextDatabase from "../interfaces/IContextDatabase";
class TrackerCollectionMongoose implements ITrackerDatabase {
    private trackerModel = mongoose.model('Tracker', TrackerSchema);
    private contextDatabase: IContextDatabase = new ContextDatabaseMongoose();

    async getById(id: string): Promise<any | null> {

        if (!this.contextDatabase.isIdValid(id)) {
            return null;
        }
        this.contextDatabase.connection();
        const result: any | null = await this.trackerModel.findById(id);
        await this.contextDatabase.disconnect();
        return result;
    }

    async getTrackersByQuery(query: {}): Promise<any[] | null> {
        this.contextDatabase.connection();
        const result: any[] | null = await this.trackerModel.find(query)
        await this.contextDatabase.disconnect();
        return result;
    }

    async getTrackerByQuery(query: {}): Promise<any | null> {
        this.contextDatabase.connection();
        const result: any | null = await this.trackerModel.findOne(query)
        await this.contextDatabase.disconnect();
        return result;

    }

    async postTracker(tracker: string): Promise<void> {

        const tracker_to_insert = new this.trackerModel({
            value: tracker,
            statistic: []
        });
        this.contextDatabase.connection();
        await tracker_to_insert.save(tracker_to_insert)
        await this.contextDatabase.disconnect();

    }

    async putTracker(data: any): Promise<void> {
        this.contextDatabase.connection();
        await this.trackerModel.replaceOne({ _id: data._id }, data);
        await this.contextDatabase.disconnect();

    }
}

export default TrackerCollectionMongoose;
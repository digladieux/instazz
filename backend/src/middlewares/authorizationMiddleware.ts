import TokenAuthentication from '../business/implements/tokenAuthenticationBusiness';
const token_authentication: TokenAuthentication = new TokenAuthentication();
import logger from '../winstonLogger';

const AuthorizationMiddleware = (req: any, res: any, next: any) => {
    const authorization: string | null = req.headers.authorization;
    if (!authorization) {
        logger.error("Authorization Header Missing");
        res.status(400).send("Authorization Header Missing");
    } else {

        const id: string | null = token_authentication.getIdFromToken(authorization);
        if (id === null) {
            logger.error("Invalid Token : " + authorization);
            res.status(400).send("Invalid Token : " + authorization);
        } else {

            req.local = {
                id: id
            }
            next();
        }
    }

}

export default AuthorizationMiddleware;
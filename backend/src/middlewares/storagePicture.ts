import { format } from 'util';
import { Storage } from '@google-cloud/storage';
import multer from 'multer';

const multerOptions = {
    storage: multer.memoryStorage(),
};
export const uploader = multer(multerOptions).single("picture");

export function savePictureGoogleCloudPlatform(req: any, res: any, next: any) {
    if (req.file == undefined) {
        return res.status(400).send('No file uploaded.');
    }

    const date: number = Date.now();
    const picture_name: string = req.file.fieldname + '-' + date + "." + req.file.originalname;
    const storage = new Storage({ keyFilename: "keys.json" });
    const bucket = storage.bucket("track-trip-pictures");
    const blob = bucket.file(picture_name);
    const blobStream = blob.createWriteStream({
        resumable: false,
    });

    blobStream.on('error', err => {
        next(err);
    });

    blobStream.on('finish', () => { });

    blobStream.end(req.file.buffer);

    return format(
        `https://storage.googleapis.com/${bucket.name}/${blob.name}`
    );
}
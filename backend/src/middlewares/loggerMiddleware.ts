import logger from '../winstonLogger'
const LoggerMiddleware = (req: Request, res: Response, next: any) => {
    logger.info(`Request ${req.method} ${req.url}`)
    next();
}

export default LoggerMiddleware;
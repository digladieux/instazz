import Tracker from "./tracker/tracker";
import Country from "./tracker/country";
import Post from "./post";
import Statistic from "./statistic";

import Comment from "./comment";
import mongoose from 'mongoose';

class GlobeTrotter extends mongoose.Document {
    private _id: string;
    private _username: string;
    private _hash_password: string;
    private _email: string;
    private _email_confirmed: boolean;
    private _first_name: string;
    private _last_name: string;
    private _biography: string;
    private _creation_date: String;
    private _birth_date: String;
    private _trackers: Array<Tracker>;
    private _country_localisation: Country;
    private _posts: Array<Post>;
    private _statistic_followers: Statistic;
    private _statistic_followed: Statistic;

    constructor(data: any) {
        super();
        this._id = data?.id;
        this._username = data?.username;
        this._hash_password = data?.hash_password;
        this._email = data?.email;
        this._email_confirmed = false;
        this._first_name = data?.first_name;
        this._last_name = data?.last_name;
        this._biography = data?.biography;
        this._creation_date = data?.creation_date;
        this._birth_date = data?.birth_date;
        this._trackers = data?.trackers;
        this._country_localisation = new Country("FR");
        // this._country_localisation = data?.country_localisation;
        this._posts = data?.posts;
        this._statistic_followers = data?._statistic_followers;
        this._statistic_followed = data?._statistic_followed;
    }

}

export default GlobeTrotter
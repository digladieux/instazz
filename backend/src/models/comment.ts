import Statistic from "./statistic";
import { Guid } from "guid-typescript";

class Comment {
    private _id: string;
    private _description: string;
    private _statistic: Statistic;
    private _id_globe_trotter: string;
    private _publication_date: Date;
    private _id_post: string;

    constructor(description: string, id_globe_trotter: string, id_post: string) {
        this._id = Guid.create().toString();
        this._description = description;
        this._statistic = new Statistic();
        this._id_globe_trotter = id_globe_trotter;
        this._publication_date = new Date();
        this._id_post = id_post;
    }
}

export default Comment;
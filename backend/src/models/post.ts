import Country from "./tracker/country";
import Hashtag from "./tracker/hashtag";
import Comment from "./comment";
import Statistic from "./statistic";
import { Guid } from "guid-typescript";
import Location from "./tracker/location";

class Post {
    private _id: string;
    private _title: string;
    private _description: string;
    private _publication_date: Date;
    private _shooting_date: Date;
    private _pictures: Array<string>;
    private _country: Country;
    private _location: Location;
    private _hashtags: Array<Hashtag>;
    private _comments: Array<Comment>;
    private _statistic: Statistic;

    constructor(title: string, description: string, shooting_date: string, pictures: string, country: string, location: string, hashtags: Array<string>) {
        this._id = Guid.create().toString();
        this._title = title;
        this._description = description;
        this._publication_date = new Date();
        this._shooting_date = new Date(shooting_date);
        this._pictures = new Array<string>(pictures);
        this._country = new Country(country);
        this._location = new Location(location);
        this._hashtags = new Array<Hashtag>();
        this._comments = new Array<Comment>();
        this._statistic = new Statistic();

        hashtags.forEach(hashtag => {
            this._hashtags.push(new Hashtag(hashtag))
        });
    }

    id(): string {
        return this._id;
    }

    comments(): Array<Comment> {
        return this._comments;
    }

    addComment(comment: Comment) {
        this._comments.push(comment);
    }
}

export default Post;
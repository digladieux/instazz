import Tracker from './tracker'

class Country extends Tracker {

    private url_flags: String = "http://localhost:65535/uploads/flags/"

    constructor(value: string) {
        super(value);
    }

    getPicture(): string {
        return this.url_flags + this._value.toLocaleLowerCase();
    }
}

export default Country;
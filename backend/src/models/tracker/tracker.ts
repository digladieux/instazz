import { Guid } from "guid-typescript";
import Statistic from "../statistic";

class Tracker {
    protected _id: string;
    protected _value: string;
    protected _statistic: Statistic;

    constructor(value: string) {
        this._id = Guid.create().toString();
        this._value = value;
        this._statistic = new Statistic();
    }
}

export default Tracker;
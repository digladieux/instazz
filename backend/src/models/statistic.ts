import { Guid } from "guid-typescript";

class Statistic {
    private _id: string;
    private _ids_globe_trotter: Array<string>;

    constructor() {
        this._id = Guid.create().toString();
        this._ids_globe_trotter = new Array<string>();
    }

    number_followers(): number {
        return this._ids_globe_trotter.length;
    }

    ids_globe_trotter(): Array<String> {
        return this._ids_globe_trotter;
    }

    id(): string {
        return this._id;
    }
}

export default Statistic;
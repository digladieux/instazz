import Swal from 'sweetalert2';
import "./../assets/style/singlePost.css"

export const AlertInput: any = () => Swal.fire({
    input: 'textarea',
    inputPlaceholder: 'Votre commentaire...',
    inputAttributes: {
        'aria-label': 'Votre commentaire'
    },
    showCancelButton: true,
    heightAuto: false
});

export const CloseLoader: any = () => {
    Swal.close();
}

export const ShowLoader: any = () => {
    Swal.fire({
        title: 'Requête au serveur',
        html: 'Fini bientôt ...',
        allowOutsideClick: false,
        onBeforeOpen: () => {
            Swal.showLoading();
        },
        customClass: {
            actions: 'centerClass'
        },
        heightAuto: false
    });
}

export const ErrorAlert: any = (text: any) => {
    return Swal.fire({
        icon: 'error',
        title: 'Quelque chose ne va pas!',
        text: text,
        confirmButtonColor: '#06cff1',
        customClass: {
            actions: 'centerClass'
        },
        heightAuto: false
    });
}

export const RequestSuccess: any = (title: string) => {
    return Swal.fire({
        title: title,
        icon: 'success',
        confirmButtonColor: '#06cff1',
        confirmButtonText: 'Ok',
        customClass: {
            actions: 'centerClass'
        },
        heightAuto: false
    });
}

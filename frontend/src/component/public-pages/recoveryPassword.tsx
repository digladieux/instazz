import RecoveryPasswordForm from '../form/recoveryPasswordForm'
import React from 'react'
import Grid from '@material-ui/core/Grid';

import "./../../assets/style/register.css" 


const RecoveryPassword = () => {
    return (
        <div className="general-form-container">
            <Grid container>
                <Grid item md={3} sm={2}>
                </Grid> 
                <Grid item md={6} sm={8}>
                    <div className="form-container">
                        <div className="form-header"><h3>Récupération du mot de passe</h3></div>
                        <div className="form">
                            <RecoveryPasswordForm />                            
                        </div>
                    </div>
                </Grid>
                <Grid item md={3} sm={2}>
                </Grid>  
            </Grid>
        </div>
            

    );
}

export default RecoveryPassword;
import React from 'react';
import queryString from 'query-string';
import LocalAuthenticationService from '../services/localAuthenticationService';
import { ErrorAlert, ShowLoader, CloseLoader, RequestSuccess } from '../alert';
import { redirection } from '../globalFunctions';

enum Context {
    confirmedEmail = "confirmedEmail"
}



class Callback extends React.Component {

    private localAuthenticationService: LocalAuthenticationService = new LocalAuthenticationService();
    private params: any;
    constructor(props: any) {
        super(props);
        const url: string = props.location?.search;
        this.params = queryString.parse(url);

    }

    async componentDidMount() {
        if (!this.params.context) {
            this.redirectionWithMessage("/", "CONTEXT Parameter missing")
        }

        else {

            switch (this.params.context) {
                case Context.confirmedEmail:
                    if (!this.params.token) {
                        this.redirectionWithMessage("/", "Token missing Parameter missing")
                    }
                    ShowLoader();
                    await this.localAuthenticationService.confirmationEmail(this.params.token)
                    CloseLoader();
                    RequestSuccess('Email confirmé');
                    redirection(this.props, '/login');
                    break;
                default:
                    this.redirectionWithMessage("/", "CONTEXT Parameter invalid")
            }
        }
    }

    private redirectionWithMessage(path: string, message: string) {
        ErrorAlert(message).then();
        window.location.replace(path);
    }

    render() {

        return (
            <div>
            </div>
        );
    }
}

export default Callback;
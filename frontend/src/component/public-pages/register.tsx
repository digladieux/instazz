import CreateUserForm from '../form/postGlobeTrotterForm'
import React from 'react'
import Grid from '@material-ui/core/Grid';

import "./../../assets/style/register.css"


class Register extends React.Component {

    render() {
        return (
            <div className="general-form-container">

                <Grid container>
                    <Grid item md={2} sm={1}>
                    </Grid>
                    <Grid item md={8} sm={10}>
                        <div className="form-container">
                            <div className="form-header"><h3>Rejoindre TrackTrip</h3></div>
                            <div className="form">
                                <CreateUserForm  {...this.props} />
                            </div>
                        </div>
                    </Grid>
                    <Grid item md={2} sm={1}>
                    </Grid>
                </Grid>
            </div>

        );
    }
}

export default Register
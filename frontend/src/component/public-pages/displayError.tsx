import React from 'react';

import "./../../assets/style/errors.css"

type Props = { errors: Map<string, string> | undefined }
class DisplayErrors extends React.Component<Props> {

    displayError() {

        let display: any = '';

        if (this.props.errors) {
            display = this.props.errors.values().next().value;
        }
        return display;
    }


    render() {
        return (
            <div className="input_errors text-center">
                {this.displayError()}
            </div>
        )
    }


}

export default DisplayErrors;
import LoginForm from '../form/loginForm'
import React from 'react'
import { Link } from 'react-router-dom';
import "./../../assets/style/login.css"

class Login extends React.Component {

    render() {

        return (
            <div className="login-page">
                <div className="container-login">
                    <div className="header-login">
                        <h3>Je m'identifie</h3>
                    </div>
                    <div className="content-login">
                        <LoginForm />
                        {/* <Link to={'/recoveryPassword'}>Mot de passe oublié?</Link> */}
                        <br />
                        <br />
                        <Link to={'/register'}>Pas encore de compte ?</Link>
                    </div>
                </div>
            </div>


        );
    }
}

export default Login
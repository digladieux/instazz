import PutPasswordForm from '../form/putPasswordForm'
import React from 'react'

import Grid from '@material-ui/core/Grid';

import "./../../assets/style/register.css" 

const ChangePassword = () => {
    return (
        <div className="general-form-container">
            <Grid container>
                <Grid item md={3} sm={2}>
                </Grid> 
                <Grid item md={6} sm={8}>
                    <div className="form-container">
                        <div className="form-header"><h3>Changer de mot de passe</h3></div>
                        <div className="form">
                            <PutPasswordForm />                         
                        </div>
                    </div>
                </Grid>
                <Grid item md={3} sm={2}>
                </Grid>  
            </Grid>
        </div>
    );
}

export default ChangePassword;
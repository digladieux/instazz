export function displayMap(map: Map<any, any> | undefined) {

    let display: any;
    if (map) {
        for (let value of map.values()) {
            display += value;
        }
    }
    return display;
}

export function displayTimeDifference(post_date: any) {
    let result: string = "Il y a ";
    let plural: string = " jours";
    let current_date: Date = new Date();
    let publication_date: Date = new Date(post_date);

    var diff = (current_date.getTime() - publication_date.getTime()) / 1000;
    diff /= (60 * 60 * 24);
    let diffInHours = Math.abs(Math.round(diff)) - 1;

    if (diffInHours === 1) {
        plural = " jour";
    }

    result += diffInHours + plural;

    if (diffInHours === 0) {
        result = "Aujourd'hui";
    }

    return result;
}


export function displayArray(array: any[]) {
    let display: any;
    if (array) {
        for (let value of array.values()) {
            display += value;
        }
    }
    return display;
}

export function isKey(dictionary: {}, key: string) {
    if (!Object.keys(dictionary).includes(key)) {
        return false;
    }
    return true;
}

export function redirection(props: any, route: string) {
    const test: any = props
    const { history } = test
    history.push(route);
}

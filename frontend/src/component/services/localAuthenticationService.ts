import axios from 'axios';
import { headersWithoutAuthorization, headersWithAuthorization } from './headers';
import { getCookie } from '../cookies';

class LocalAuthenticationService {

    private url_api: string;

    constructor() {
        if (process.env.REACT_APP_API_URI) {
            this.url_api = process.env.REACT_APP_API_URI + "authentication/";
        } else {
            throw new ReferenceError("REACT_APP_API_URI not find");
        }
    }

    async postGlobeTrotter(data: any, picture: any) {

        try {
            var formData: FormData = new FormData();
            formData.set('username', data.username);
            formData.set('password', data.password);
            formData.set('email', data.email);
            formData.set('first_name', data.first_name);
            formData.set('last_name', data.last_name);
            formData.set('birth_date', data.birth_date);
            formData.set('biography', data.biography);
            formData.set('country', data.country);
            formData.append('picture', picture);
            return await axios.post(this.url_api + "globeTrotter", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
        } catch (error) {
            return error;
        }
    }

    async loginByCredentials(username: string, password: string) {
        try {
            return await axios.get(this.url_api + "/loginByCredentials", {
                headers: headersWithoutAuthorization,
                params: {
                    username: username,
                    password: password
                }
            });
        } catch (error) {
            return error;
        }
    }

    async loginByToken(token: string) {
        try {
            return await axios.get(this.url_api + "/loginByToken", {
                headers: headersWithoutAuthorization,
                params: {
                    token: token,
                }
            });
        } catch (error) {
            return error;
        }
    }

    async resetPassword(email: string) {
        try {
            return await axios({
                method: 'get',
                url: this.url_api + '/resetPassword',
                params: {
                    email: email,
                },
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: getCookie('authorization'),
                },
            })
        } catch (error) {
            return error;
        }
    }

    async putPassword(old_password: string, new_password: string) {
        try {
            return await axios.get(this.url_api + "/putPassword", {
                headers: headersWithAuthorization,
                params: {
                    old_password: old_password,
                    new_password: new_password,
                }
            });
        } catch (error) {
            return error;
        }
    }

    async confirmationEmail(token: string) {
        try {
            return await axios.post(this.url_api + '/confirmationEmail',
                {
                    token: token,
                }, {
                headers: headersWithoutAuthorization,
            });
        } catch (error) {
            return error;
        }
    }
}

export default LocalAuthenticationService;
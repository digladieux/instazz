import { getCookie } from "../cookies";

export var headersWithoutAuthorization = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
}

export function headersWithAuthorization() {
    return {
        'Access-Control-Allow-Origin': true,
        'Content-Type': 'application/json',
        Authorization: getCookie('authorization')
    }
}
import axios from 'axios';
import { headersWithAuthorization } from './headers';
import { getCookie } from '../cookies';
class GlobeTrotterService {

    private url_api: string;

    constructor() {
        if (process.env.REACT_APP_API_URI) {
            this.url_api = process.env.REACT_APP_API_URI + "globeTrotter";
        } else {
            throw new ReferenceError("REACT_APP_API_URI not find");
        }
    }

    async putGlobeTrotter(data: any) {
        try {
            return await axios({
                method: 'put',
                url: this.url_api,
                data: {
                    username: data.username,
                    email: data.email,
                    first_name: data.first_name,
                    last_name: data.last_name,
                    country: data.country,
                    birth_date: data.birth_date,
                },
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: getCookie('authorization'),
                },
            })
        } catch (error) {
            return error;
        }
    }


    async getGlobeTrotterById(id: string) {
        try {
            return await axios({
                method: 'get',
                url: this.url_api + '/user/:id',
                params: {
                    id: id,
                },
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: getCookie('authorization'),
                },
            })
        } catch (error) {
            return error;
        }
    }

    async getMe() {
        try {
            return await axios.get(this.url_api + '/me', {
                headers: headersWithAuthorization(),
            });
        } catch (error) {
            return error;
        }
    }


    async postLike(data: any) {
        try {
            return await axios({
                method: 'post',
                url: this.url_api + "/postLike",
                data: {
                    id_globe_trotter: data,
                },
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: getCookie('authorization'),
                },
            })
        } catch (error) {
            return error;
        }
    }

    async deleteGlobeTrotter() {
        try {
            await axios.delete(this.url_api, {
                params: {
                    id: 'id'
                }, headers: headersWithAuthorization
            })
        } catch (error) {
            return error;
        }
    }
}

export default GlobeTrotterService;
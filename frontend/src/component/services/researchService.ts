import axios from 'axios';
import { getCookie } from '../cookies';

class ResearchService {

    private url_api: string;

    constructor() {
        if (process.env.REACT_APP_API_URI) {
            this.url_api = process.env.REACT_APP_API_URI + "research";
        } else {
            throw new ReferenceError("REACT_APP_API_URI not find");
        }
    }

    async getResearch(research: string) {
        try {
            return await axios({
                method: 'get',
                url: this.url_api,
                params: {
                    research: research,
                },
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: getCookie('authorization'),
                },
            })
        } catch (error) {
            return error;
        }
    }

}
export default ResearchService;
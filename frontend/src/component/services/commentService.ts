import axios from 'axios';
import { headersWithAuthorization } from './headers';
import { getCookie } from '../cookies';

class CommentService {

    private url_api: string;

    constructor() {
        if (process.env.REACT_APP_API_URI) {
            this.url_api = process.env.REACT_APP_API_URI + "comment";
        } else {
            throw new ReferenceError("REACT_APP_API_URI not find");
        }
    }
    async getComments(comment: any) {
        try {
            return await axios.get(this.url_api, {
                params: {
                    id_post: comment.id_post,
                    page_number: comment.page_number,
                    page_size: comment.page_size
                }, headers: headersWithAuthorization
            })
        } catch (error) {
            return error;
        }
    }

    async postComment(data: any) {
        try {
            return await axios({
                method: 'post',
                url: this.url_api,
                data: {
                    id_post: data.id_post,
                    description: data.description,
                },
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: getCookie('authorization'),
                },
            })
        } catch (error) {
            return error;
        }
    }

    async postLike(data: any) {
        try {
            return await axios.post(this.url_api + "/postLike", {
                id_comment: data.id_comment,
                id_post: data.id_post,
            }, {
                headers: headersWithAuthorization
            })
        } catch (error) {
            return error;
        }
    }

    async deleteComment(id_comment: string) {

        try {
            await axios.delete(this.url_api, {
                params: {
                    id: id_comment
                }, headers: headersWithAuthorization
            })
        } catch (error) {
            return error;
        }

    }
}

export default CommentService;
import React from 'react';
import { isKey } from '../globalFunctions';
import ResearchService from '../services/researchService';
import RequiredValidation from '../validator/requiredValidation';
import GlobeTrotterModel from '../models/globeTrotterModel';
type State = {
    research: string,
};

type Props = {
    onChange: (data: any) => void
}
class ResearchForm extends React.Component<Props, State> {

    private researchService: ResearchService = new ResearchService();
    private results: { globe_trotter: GlobeTrotterModel[], trackers: string[] };
    constructor(props: any) {
        super(props);
        this.state = {
            research: '',
        };
        this.results = { globe_trotter: [], trackers: [] };
    }

    myChangeHandler = (event: any) => {
        let nam = event.target.name;
        let val = event.target.value;
        if (!isKey(this.state, nam)) {
            throw new Error("Invalid Input");
        }
        this.setState({ [nam]: val } as Pick<State, keyof State>);
    }

    async componentDidUpdate() {
        if (!new RequiredValidation(this.state.research).isValid()) {
            this.results = { globe_trotter: [], trackers: [] };
        } else {
            const result = await this.researchService.getResearch(this.state.research);
            if (result.status === 200) {
                this.results = result.data
                this.props.onChange(this.results);
            } else {
                this.results = { globe_trotter: [], trackers: [] };
            }
        }
    }

    render() {
        return (
            <div>
                <input
                        id='research'
                        type='text'
                        name='research'
                        placeholder='Rechercher'
                        value={this.state.research}
                        onChange={this.myChangeHandler} />
                <br></br>
            </div>
        );
    }
}
export default ResearchForm;
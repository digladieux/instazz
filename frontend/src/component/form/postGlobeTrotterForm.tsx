import React from 'react';
import PasswordValidator from '../validator/passwordValidation';
import EmailValidation from '../validator/emailValidation';
import AlphabetValidation from '../validator/alphabetValidation';
import EqualValueValidation from '../validator/equalValueValidation';
import LengthValidation from '../validator/lengthValidation';
import { redirection } from '../globalFunctions';
import { ShowLoader, ErrorAlert, CloseLoader, RequestSuccess } from '../alert';
import LocalAuthenticationService from '../services/localAuthenticationService';
import ReactFlagsSelect from 'react-flags-select';
import Grid from '@material-ui/core/Grid';
import 'react-flags-select/css/react-flags-select.css';
import "../../assets/style/selectFlag.css";
import "./../../assets/style/form.css"
import DisplayErrors from '../public-pages/displayError';
import { getCookie } from '../cookies';


type State = {
    username: string,
    password: string,
    check_password: string,
    email: string,
    first_name: string,
    last_name: string,
    birth_date: string,
    biography: string,
    profile_picture: string,
    country: string
};
class PostGlobeTrotterForm extends React.Component<{}, State> {

    private _localService: LocalAuthenticationService = new LocalAuthenticationService();;
    private errors: Map<string, Map<string, string>> = new Map();

    constructor(props: any) {
        super(props);
        this.state = {
            username: '',
            password: '',
            check_password: '',
            email: '',
            first_name: '',
            last_name: '',
            birth_date: '',
            biography: '',
            profile_picture: '',
            country: 'FR',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onSelectCountry = this.onSelectCountry.bind(this);

    }

    async componentDidMount() {
        if (getCookie('authorization') !== undefined) {

        }
    }

    myChangeHandler = (event: any) => {
        let nam = event.target.name;
        let val = event.target.value;

        this.setState({ [nam]: val } as Pick<State, keyof State>);
        switch (nam) {
            case "password":
                this.errors.set("password", new PasswordValidator(val).errors());
                break;
            case "check_password":
                this.errors.set("check_password", new EqualValueValidation(val, this.state.password).errors());
                break;
            case "username":
                this.errors.set("username", new LengthValidation(val, 6).errors());
                break;
            case "email":
                this.errors.set("email", new EmailValidation(val).errors());
                break;
            case "first_name":
                this.errors.set("first_name", new AlphabetValidation(val).errors());
                break;
            case "last_name":
                this.errors.set("last_name", new AlphabetValidation(val).errors());
                break;
        }

        this.isFormValid();


    }

    isFormValid() {

        (document.getElementById("submit") as HTMLButtonElement).disabled =
            !new PasswordValidator(this.state.password).isValid() ||
            !new EqualValueValidation(this.state.password, this.state.check_password).isValid() ||
            !new LengthValidation(this.state.username, 6).isValid() ||
            !new EmailValidation(this.state.email).isValid() ||
            !new AlphabetValidation(this.state.first_name).isValid() ||
            !new AlphabetValidation(this.state.last_name).isValid() ||
            (document.getElementById("picture") as HTMLInputElement).files?.length === 0
    }

    async handleSubmit(event: any) {
        event.preventDefault();
        ShowLoader();
        var picture = (document.getElementById("picture") as HTMLInputElement).files;
        if (picture != null) {
            const result = await this._localService.postGlobeTrotter(this.state, picture[0]);
            if (result.status !== 201) {
                ErrorAlert(result.response.data).then();
            } else {
                CloseLoader();
                RequestSuccess("Account created").then();
                redirection(this.props, '/login');
            }
        }
    }

    onSelectCountry(country_code: string) {
        this.setState({ country: country_code } as Pick<State, keyof State>);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <Grid container>
                    <Grid item xs={12} sm={12} md={6}>
                        <label>Nom d'utilisateur <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='username'
                                type='text'
                                name='username'
                                placeholder=''
                                onChange={this.myChangeHandler} />
                            <DisplayErrors errors={this.errors.get('username')} />
                        </label>
                        <br />
                        <label>Prénom <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='first_name'
                                type='text'
                                name='first_name'
                                placeholder=''
                                onChange={this.myChangeHandler} />
                            <DisplayErrors errors={this.errors.get('first_name')} />
                        </label>
                        <br />
                        <label>Nom <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='last_name'
                                type='text'
                                name='last_name'
                                placeholder=''
                                onChange={this.myChangeHandler} />
                            <DisplayErrors errors={this.errors.get('last_name')} />
                        </label>
                        <br />
                        <label>Adresse mail <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='email'
                                type='email'
                                name='email'
                                placeholder=''
                                onChange={this.myChangeHandler} />
                            <DisplayErrors errors={this.errors.get('email')} />
                        </label>
                        <br />
                    </Grid>
                    <Grid item xs={12} sm={12} md={6}>
                        <label>Entrez votre mot de passe <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='password'
                                type='password'
                                name='password'
                                placeholder=''
                                onChange={this.myChangeHandler} />
                            <DisplayErrors errors={this.errors.get('password')} />
                        </label>
                        <br />
                        <label>Entrez à nouveau votre mot de passe <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='check_password'
                                type='password'
                                name='check_password'
                                placeholder=''
                                onChange={this.myChangeHandler} />
                            <DisplayErrors errors={this.errors.get('check_password')} />
                        </label>
                        <br />


                        <label>Date de naissance <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='birth_date'
                                type='date'
                                name='birth_date'
                                onChange={this.myChangeHandler} />
                        </label>
                        <br />
                        <label>Écrivez votre courte description
                            <br />
                            <input
                                className='textInput'
                                type='text'
                                name='biography'
                                placeholder=''
                                onChange={this.myChangeHandler} />
                        </label>
                        <br />
                    </Grid>
                    <Grid item xs={12} lg={12}>
                        <div style={{ width: 25 + '%' }}>
                            <ReactFlagsSelect
                                searchable={true}
                                defaultCountry="FR"
                                searchPlaceholder="Rechercher un pays"
                                onSelect={this.onSelectCountry}
                                placeholder="Sélectionner votre pays"
                                className="flag-select" />
                        </div>
                    </Grid>
                    <Grid item xs={12} lg={12}>
                        <input type="file" id="picture" name="picture" onChange={this.myChangeHandler} />

                    </Grid>
                    <Grid item xs={12} lg={12}>
                        <input id="submit" type="submit" value="Submit" disabled />
                    </Grid>
                </Grid>
            </form>
        );
    }
}
export default PostGlobeTrotterForm;
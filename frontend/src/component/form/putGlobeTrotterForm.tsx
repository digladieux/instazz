import React from 'react';
import EmailValidation from '../validator/emailValidation';
import AlphabetValidation from '../validator/alphabetValidation';
import LengthValidation from '../validator/lengthValidation';
import { redirection } from '../globalFunctions';
import { ShowLoader, ErrorAlert, CloseLoader, RequestSuccess } from '../alert';
import ReactFlagsSelect from 'react-flags-select';
import Grid from '@material-ui/core/Grid';

import "../../assets/style/selectFlag.css";
import "./../../assets/style/form.css"
import DisplayErrors from '../public-pages/displayError';
import GlobeTrotterModel from '../models/globeTrotterModel';
import GlobeTrotterService from '../services/globeTrotterService';

type Props = {
    globeTrotter: GlobeTrotterModel
}
type State = {
    username: string,
    email: string,
    first_name: string,
    last_name: string,
    birth_date: string,
    biography: string,
    country: string
};
class PutGlobeTrotterForm extends React.Component<Props, State> {

    private globeTrotterService: GlobeTrotterService = new GlobeTrotterService();;
    private errors: Map<string, Map<string, string>> = new Map();

    constructor(props: any) {
        super(props);
        this.state = {
            username: this.props.globeTrotter.username,
            email: this.props.globeTrotter.email,
            first_name: this.props.globeTrotter.first_name,
            last_name: this.props.globeTrotter.last_name,
            birth_date: this.props.globeTrotter.birth_date,
            biography: this.props.globeTrotter.biography,
            country: this.props.globeTrotter.country_localisation,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onSelectCountry = this.onSelectCountry.bind(this);

    }

    myChangeHandler = (event: any) => {
        let nam = event.target.name;
        let val = event.target.value;

        this.setState({ [nam]: val } as Pick<State, keyof State>);
        switch (nam) {
            case "username":
                this.errors.set("username", new LengthValidation(val, 6).errors());
                break;
            case "email":
                this.errors.set("email", new EmailValidation(val).errors());
                break;
            case "first_name":
                this.errors.set("first_name", new AlphabetValidation(val).errors());
                break;
            case "last_name":
                this.errors.set("last_name", new AlphabetValidation(val).errors());
                break;
        }

        this.isFormValid();


    }

    isFormValid() {

        (document.getElementById("submit") as HTMLButtonElement).disabled =
            !new LengthValidation(this.state.username, 6).isValid() ||
            !new EmailValidation(this.state.email).isValid() ||
            !new AlphabetValidation(this.state.first_name).isValid() ||
            !new AlphabetValidation(this.state.last_name).isValid() ||
            (document.getElementById("picture") as HTMLInputElement).files?.length === 0
    }

    async handleSubmit(event: any) {
        event.preventDefault();
        ShowLoader();
        const result = await this.globeTrotterService.putGlobeTrotter(this.state);
        if (result.status !== 200) {
            ErrorAlert(result.response.data).then();
        } else {
            CloseLoader();
            RequestSuccess("Account updated").then();
            redirection(this.props, '/login');
        }
    }

    onSelectCountry(country_code: string) {
        this.setState({ country: country_code } as Pick<State, keyof State>);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <Grid container>
                    <Grid item xs={12} sm={12} md={6}>
                        <label>Nom d'utilisateur <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='username'
                                type='text'
                                name='username'
                                placeholder={this.state.username}
                                onChange={this.myChangeHandler} />
                            <DisplayErrors errors={this.errors.get('username')} />
                        </label>
                        <br />
                        <label>Prénom <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='first_name'
                                type='text'
                                name='first_name'
                                placeholder={this.state.first_name}
                                onChange={this.myChangeHandler} />
                            <DisplayErrors errors={this.errors.get('first_name')} />
                        </label>
                        <br />
                        <label>Nom <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='last_name'
                                type='text'
                                name='last_name'
                                placeholder={this.state.last_name}
                                onChange={this.myChangeHandler} />
                            <DisplayErrors errors={this.errors.get('last_name')} />
                        </label>
                        <br />
                        <label>Adresse mail <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='email'
                                type='email'
                                name='email'
                                placeholder={this.state.email}
                                onChange={this.myChangeHandler} />
                            <DisplayErrors errors={this.errors.get('email')} />
                        </label>
                        <br />
                    </Grid>
                    <Grid item xs={12} sm={12} md={6}>
                        <label>Date de naissance <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                id='birth_date'
                                type='date'
                                name='birth_date'
                                placeholder={this.state.birth_date}
                                onChange={this.myChangeHandler} />
                        </label>
                        <br />
                        <label>Écrivez votre courte description <span className="star">*</span>
                            <br />
                            <input
                                className='textInput'
                                type='text'
                                name='biography'
                                placeholder={this.state.biography}
                                onChange={this.myChangeHandler} />
                        </label>
                        <br />
                    </Grid>
                    <Grid item xs={12} lg={12}>
                        <div style={{ width: 25 + '%' }}>
                            <ReactFlagsSelect
                                defaultCountry="FR"
                                onSelect={this.onSelectCountry}
                                placeholder="Sélectionner votre pays"
                                className="flag-select" />
                        </div>
                    </Grid>
                    <Grid item xs={12} lg={12}>
                        <input id="submit" type="submit" value="Submit" disabled />
                    </Grid>
                </Grid>
            </form>
        );
    }
}
export default PutGlobeTrotterForm;
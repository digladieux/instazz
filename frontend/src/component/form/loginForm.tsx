import React from 'react';
import { isKey, redirection } from '../globalFunctions';
import RequiredValidation from '../validator/requiredValidation';
import { ShowLoader, CloseLoader, ErrorAlert } from '../alert';
import { withRouter } from 'react-router-dom'
import { setCookie, getCookie, removeCookie } from '../cookies';
import "./../../assets/style/form.css"
import DisplayErrors from '../public-pages/displayError';
import LocalAuthenticationService from '../services/localAuthenticationService';
import GlobeTrotterService from '../services/globeTrotterService';

type State = { username: string, password: string };
class LoginForm extends React.Component<{}, State> {

    private errors: Map<string, Map<string, string>> = new Map();
    private localAuthenticationService: LocalAuthenticationService = new LocalAuthenticationService();
    private globeTrotterService: GlobeTrotterService = new GlobeTrotterService();
    constructor(props: any) {
        super(props);
        this.state = {
            username: '',
            password: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);


    }

    async componentDidMount() {
        const authorization = getCookie('authorization')
        if (authorization !== undefined) {
            const result = await this.localAuthenticationService.loginByToken(authorization);
            if (result.status === 200) {
                redirection(this.props, '/dashboard/trackersFeed')
            } else {
                removeCookie('authorization')
                removeCookie('id')
            }

        }
    }

    myChangeHandler = (event: any) => {
        let nam = event.target.name;
        let val = event.target.value;
        if (!isKey(this.state, nam)) {

            throw new Error("Invalid Input");
        }
        this.setState({ [nam]: val } as Pick<State, keyof State>);
        switch (nam) {
            case "password":
                this.errors.set("password", new RequiredValidation(val).errors());
                break;
            case "username":
                this.errors.set("username", new RequiredValidation(val).errors());
                break;
        }

        this.isFormValid();


    }


    isFormValid() {
        if (
            new RequiredValidation(this.state.password).isValid() &&
            new RequiredValidation(this.state.username).isValid()
        ) {
            (document.getElementById("submit") as HTMLButtonElement).disabled = false;

        } else {
            (document.getElementById("submit") as HTMLButtonElement).disabled = true;
        }

    }

    async handleSubmit(event: any) {
        event.preventDefault();
        ShowLoader();
        const result = await this.localAuthenticationService.loginByCredentials(this.state.username, this.state.password)
        if (result.status !== 200) {
            ErrorAlert(result.response.data).then();
        } else {
            setCookie('authorization', result.data)
            const id = await this.globeTrotterService.getMe();
            setCookie('id', id.data._id);
            CloseLoader();
            redirection(this.props, '/dashboard/trackersFeed');
        }
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label><span>Nom d'utilisateur <span className="star">*</span></span>
                    <br />
                    <input
                        className="textInput"
                        id='username'
                        type='text'
                        name='username'
                        placeholder=""
                        onChange={this.myChangeHandler} />
                    <DisplayErrors errors={this.errors.get('username')} />
                </label><br></br>
                <label><span>Mot de passe <span className="star">*</span></span>
                    <br />
                    <input
                        className="textInput"
                        id='password'
                        type='password'
                        name='password'
                        placeholder=''
                        onChange={this.myChangeHandler} />
                    <DisplayErrors errors={this.errors.get('password')} />
                </label><br></br>
                <input id="submit"
                    type="submit" value="Se connecter" disabled />
            </form>

        );
    }
}
export default withRouter(LoginForm);
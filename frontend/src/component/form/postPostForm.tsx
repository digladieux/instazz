import 'react-flags-select/css/react-flags-select.css';
import ReactFlagsSelect from 'react-flags-select';
import React from 'react';
import PostService from '../services/postService';
import { ShowLoader, ErrorAlert, CloseLoader } from '../alert';
import { isKey, redirection } from '../globalFunctions';
import RequiredValidation from '../validator/requiredValidation';
import DisplayErrors from '../public-pages/displayError';

import "../../assets/style/selectFlag.css";
import "../../assets/style/formNewPost.css";

import DateRangeIcon from '@material-ui/icons/DateRange';
import LocationOnIcon from '@material-ui/icons/LocationOn';


type State = {
    title: string,
    description: string,
    shooting_date: string,
    country: string,
    location: string,
};
class PostPostForm extends React.Component<{}, State> {

    private postService: PostService = new PostService();
    private errors: Map<string, Map<string, string>> = new Map();

    constructor(props: any) {

        super(props);
        this.state = {
            title: '',
            description: '',
            shooting_date: '',
            country: 'FR',
            location: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onSelectCountry = this.onSelectCountry.bind(this);
    }

    myChangeHandler = (event: any) => {
        let nam = event.target.name;
        let val = event.target.value;
        if (!isKey(this.state, nam)) {
            if (nam === 'picture') {
            }
            else {
                throw new Error("Invalid Input");
            }
        } else {
            this.setState({ [nam]: val } as Pick<State, keyof State>);
            switch (nam) {
                case "title":
                    this.errors.set("title", new RequiredValidation(val).errors());
                    break;
                case "country":
                    this.errors.set("country", new RequiredValidation(val).errors());
                    break;
            }


        }
    }

    isFormValid() {
        if (
            new RequiredValidation(this.state.title).isValid() &&
            new RequiredValidation(this.state.country).isValid() &&
            (document.getElementById("picture") as HTMLInputElement).files?.length !== 0

        ) {
            (document.getElementById("submit") as HTMLButtonElement).disabled = false;

        } else {
            (document.getElementById("submit") as HTMLButtonElement).disabled = true;
        }

    }

    async handleSubmit(event: any) {
        event.preventDefault();
        ShowLoader();
        const val = (document.getElementById("picture") as HTMLInputElement)
        const result = await this.postService.postPost(this.state, val?.files);
        if (result.status !== 201) {
            ErrorAlert(result?.response?.data).then();
        } else {
            CloseLoader();
            redirection(this.props, '/dashboard/trackersFeed');

        }
    }

    onSelectCountry(country_code: string) {
        this.setState({ country: country_code } as Pick<State, keyof State>);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    <input
                        id='title'
                        type='text'
                        name='title'
                        placeholder='Entrez votre titre...'
                        onChange={this.myChangeHandler} />
                    <DisplayErrors errors={this.errors.get('title')} />
                </label><br></br>
                <label>
                    <input
                        id='description'
                        type='text'
                        name='description'
                        placeholder='Entrez votre description...'
                        onChange={this.myChangeHandler} />
                    <DisplayErrors errors={this.errors.get('description')} />
                </label><br></br>
                <div className="new-post-input shooting_date">
                    <DateRangeIcon className="icone" color="secondary" style={{ fontSize: 30 }} />
                    <div className="one-input-form-container">
                        <label>
                            <input
                                id='shooting_date'
                                type='date'
                                name='shooting_date'
                                onChange={this.myChangeHandler} />
                        </label><br></br>
                    </div>

                </div>
                <div className="new-post-input location">
                    <LocationOnIcon className="icone" color="secondary" style={{ fontSize: 30 }} />
                    <div className="one-input-form-container">
                        <label>
                            <input
                                id='location'
                                type='text'
                                name='location'
                                placeholder='Entrez le lieu'
                                onChange={this.myChangeHandler} />
                        </label><br></br>
                    </div>

                </div>
                <div className="pays">
                    <div style={{ width: 25 + '%' }}>
                        <ReactFlagsSelect
                            searchable={true}
                            defaultCountry="FR"
                            searchPlaceholder="Rechercher un pays"
                            onSelect={this.onSelectCountry}
                            placeholder="Sélectionner votre pays"
                            className="flag-select" />
                    </div>
                </div>

                <input type="file" id="picture" name="picture" onChange={this.myChangeHandler} />
                <br />
                <input id="submit" className="post-submit"
                    type="submit" value="Submit" />
            </form>
        );
    }
}
export default PostPostForm;

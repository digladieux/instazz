import React from 'react';
import EmailValidation from '../validator/emailValidation';
import { ShowLoader, ErrorAlert, CloseLoader } from '../alert';
import { isKey, displayMap, redirection } from '../globalFunctions';

import "./../../assets/style/form.css"
import LocalAuthenticationService from '../services/localAuthenticationService';


type State = {
    email: string,
};
class RecoveryPasswordForm extends React.Component<{}, State> {

    private localService: LocalAuthenticationService = new LocalAuthenticationService()
    private errors: Map<string, string> = new Map();

    constructor(props: any) {
        super(props);
        this.state = {
            email: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    myChangeHandler = (event: any) => {
        let nam = event.target.name;
        let val = event.target.value;
        if (!isKey(this.state, nam)) {

            throw new Error("Invalid Input");
        }
        this.setState({ [nam]: val } as Pick<State, keyof State>);
        this.errors = new EmailValidation(val).errors();
        this.isFormValid();
    }

    isFormValid() {
        if (new EmailValidation(this.state.email).isValid()) {
            (document.getElementById("submit") as HTMLButtonElement).disabled = false;

        } else {
            (document.getElementById("submit") as HTMLButtonElement).disabled = true;
        }

    }

    async handleSubmit(event: any) {
        event.preventDefault();
        ShowLoader();
        const result = await this.localService.resetPassword(this.state.email);
        if (result.status !== 200) {
            ErrorAlert(result.response.data).then();
        } else {
            CloseLoader();
            redirection(this.props, '/login');
        }
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>Entrez votre email:
                <br />
                    <input
                        className="textInput"
                        id='email'
                        type='email'
                        name='email'
                        placeholder=''
                        onChange={this.myChangeHandler} />
                    {displayMap(this.errors)}
                </label><br></br>
                <input id="submit"
                    type="submit" value="Submit" disabled />
            </form>
        );
    }
}
export default RecoveryPasswordForm;
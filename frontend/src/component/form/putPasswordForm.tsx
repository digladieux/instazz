import React from 'react';
import PasswordValidator from '../validator/passwordValidation';
import EqualValueValidation from '../validator/equalValueValidation';
import RequiredValidation from '../validator/requiredValidation';
import { ShowLoader, ErrorAlert, CloseLoader } from '../alert';
import { displayMap, isKey, redirection } from '../globalFunctions';
import "./../../assets/style/form.css"
import LocalAuthenticationService from '../services/localAuthenticationService';

type State = {
    old_password: string,
    new_password: string,
    check_password: string,
};
class PutPasswordForm extends React.Component<{}, State> {

    private errors: Map<string, Map<string, string>> = new Map();
    private localService: LocalAuthenticationService = new LocalAuthenticationService()
    constructor(props: any) {
        super(props);
        this.state = {
            old_password: '',
            new_password: '',
            check_password: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    myChangeHandler = (event: any) => {
        let nam = event.target.name;
        let val = event.target.value;
        if (!isKey(this.state, nam)) {

            throw new Error("Invalid Input");
        }
        this.setState({ [nam]: val } as Pick<State, keyof State>);
        switch (nam) {
            case "old_password":
                this.errors.set("old_password", new RequiredValidation(val).errors());
                break;
            case "check_password":
                this.errors.set("check_password", new EqualValueValidation(val, this.state.new_password).errors());
                break;
            case "new_password":
                this.errors.set("new_password", new PasswordValidator(val).errors());
                break;
        }

        this.isFormValid();


    }

    isFormValid() {
        if (
            new RequiredValidation(this.state.old_password).isValid() &&
            new PasswordValidator(this.state.new_password).isValid() &&
            new EqualValueValidation(this.state.new_password, this.state.check_password).isValid()
        ) {
            (document.getElementById("submit") as HTMLButtonElement).disabled = false;

        } else {
            (document.getElementById("submit") as HTMLButtonElement).disabled = true;
        }

    }

    async handleSubmit(event: any) {
        event.preventDefault();
        ShowLoader();
        const result = await this.localService.putPassword(this.state.old_password, this.state.new_password);
        if (result.status !== 200) {
            ErrorAlert(result.response.data).then();
        } else {
            CloseLoader();
            redirection(this.props, '/login');

        }
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>Entrez votre ancien mot de passe:
                <input
                        className="textInput"
                        id='old_password'
                        type='password'
                        name='old_password'
                        placeholder=''
                        onChange={this.myChangeHandler} />
                    {displayMap(this.errors.get('old_password'))}
                </label><br></br>
                <label>Entrez votre nouveau mot de passe:
                <input
                        className="textInput"
                        id='new_password'
                        type='password'
                        name='new_password'
                        placeholder=''
                        onChange={this.myChangeHandler} />
                    {displayMap(this.errors.get('new_password'))}
                </label><br></br>
                <label>Entrez encore votre nouveau mot de passe:
                <input
                        className="textInput"
                        id='check_password'
                        type='password'
                        name='check_password'
                        placeholder=''
                        onChange={this.myChangeHandler} />
                    {displayMap(this.errors.get('check_password'))}
                </label><br></br>
                <input id="submit"
                    type="submit" value="Submit" disabled />
            </form>
        );
    }
}
export default PutPasswordForm;
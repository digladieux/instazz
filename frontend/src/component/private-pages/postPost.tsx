import React from 'react';
import PostPostForm from '../form/postPostForm';
import "./../../assets/style/postPost.css";


type state = { picture: any, error: string };
class PostPost extends React.Component<{}, state>  {

    constructor(props: any) {
        super(props);
        this.state = {
            picture: '',
            error: ''
        }
    }

    render() {
        return (
            <div className="post-background">
                <div className="form-container">
                    <div className="form-header">
                        Créer une  nouvelle publication:
                    </div>
                    <div className="form-inputs">
                        <PostPostForm {...this.props} />
                    </div>

                </div>
                
            </div>

        );
    }
}


export default PostPost;
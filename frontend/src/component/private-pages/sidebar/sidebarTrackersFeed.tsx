import React from "react";
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import '../../../assets/style/nav.css';
import ResearchForm from "../../form/researchForm";
import GlobeTrotterModel from "../../models/globeTrotterModel";
import { Divider } from "@material-ui/core";
import { Link } from 'react-router-dom';

type State = {
    globe_trotters: GlobeTrotterModel[];
}

class SidebarTrackersFeed extends React.Component<{}, State>  {


    constructor(props) {
        super(props)

        this.state = {
            globe_trotters: []
        }
        this.fetchData = this.fetchData.bind(this);
    }

    fetchData(data: any) {
        this.setState({ globe_trotters: data.globe_trotter })
    }

    UserList() {
        let listUser: Array<any> = new Array<any>();

        this.state.globe_trotters.forEach(element => {
            listUser.push(this.displayUser(element));
        });

        return (
            <>
                {listUser}
            </>
        );
    }

    displayUser(globe_trotter: GlobeTrotterModel) {
        return (
            <Card className="user-item" key={globe_trotter._id}>
                <CardHeader
                    className="user-header"
                    avatar={
                        <Link to={{ pathname: `/dashboard/singleGlobeTrotter?id=${globe_trotter._id}` }} className="user-link">
                            <Avatar alt={String(globe_trotter.username)} src={String(globe_trotter.profile_picture)}>
                            </Avatar>
                        </Link>
                    }
                    title={globe_trotter.username}
                />
            </Card>
        )

    }

    render() {

        return (
            <>
                <ResearchForm onChange={this.fetchData} />
                <Divider />
                <div className="item-container">
                    <div className="title-container">GlobeTrotters:</div>
                    <div className="container item-list" id="globe_trotters">
                        {this.UserList()}
                    </div>
                </div>
            </>
        );
    }
}

export default SidebarTrackersFeed;
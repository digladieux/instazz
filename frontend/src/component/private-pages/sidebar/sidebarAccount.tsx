import React from "react";
import { Tooltip } from '@material-ui/core';

import '../../../assets/style/nav.css';
import GlobeTrotterModel from "../../models/globeTrotterModel";
import { ShowLoader, CloseLoader } from "../../alert";
import GlobeTrotterService from "../../services/globeTrotterService";
import { getCookie } from "../../cookies";

type State = { id: string }
type Props = { globe_trotter: GlobeTrotterModel }
class SidebarAccount extends React.Component<Props, State>  {

  private globeTrotterService: GlobeTrotterService = new GlobeTrotterService();
  constructor(props) {
    super(props);
    this.state = {
      id: getCookie('id')
    }
  }
  render() {

    var username = '';
    var profile_picture = '';
    var last_name = '';
    var first_name = '';
    var biography = '';
    var posts;
    var statistic_followers;
    var statistic_followed;
    var id = '';
    if (this.props.globe_trotter) {
      id = String(this.props.globe_trotter._id);
      username = String(this.props.globe_trotter.username);
      profile_picture = String(this.props.globe_trotter.profile_picture);
      last_name = String(this.props.globe_trotter.last_name);
      first_name = String(this.props.globe_trotter.first_name);
      biography = String(this.props.globe_trotter.biography);
      posts = this.props.globe_trotter?.posts;
      statistic_followers = this.props.globe_trotter?.statistic_followers;
      statistic_followed = this.props.globe_trotter?.statistic_followed;
    }

    return (
      <>
        <div className="account-photo">
          <img src={profile_picture} alt="profil" />
        </div>

        <div className="user-pseudo">
          {username}
        </div>

        <div className="user-last-first-name">
          {last_name} {first_name}
        </div>

        <div className="user-biography-style">
          <Tooltip title={biography}>
            <div className="user-biography-text">
              {biography}<br></br>
            </div>
          </Tooltip>
        </div>

        {getCookie('id') !== id ?
          <button className="follow" onClick={() => { ShowLoader(); this.globeTrotterService.postLike(id); CloseLoader() }}>
            {statistic_followed !== undefined && statistic_followed.length !== 0 && statistic_followed.find((id_stat: string) => id_stat === this.state.id) ? <div className="gray">Ne plus suivre</div> : 'Suivre'}
          </button>
          : ''}

        <div className="user-stats">
          {posts ? posts.length : 0} publications <br /><br />
          {statistic_followers ? statistic_followers.length : 0} abonnements <br /> <br />
          {statistic_followed ? statistic_followed.length : 0} suiveurs
        </div>
      </>
    );
  }
}

export default SidebarAccount;
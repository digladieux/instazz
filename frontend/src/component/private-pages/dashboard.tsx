import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Redirect } from 'react-router-dom'
import { getCookie } from '../cookies';
import TrackersFeed from './trackersFeed';
import SingleGlobeTrotter from './singleGlobeTrotter';
import PostPost from './postPost';
import Account from './account';
import LargeNav from './navs/largeNav';
import TopSmallNav from './navs/topSmallNav';
import BottomSmallNav from './navs/bottomSmallNav';
import { Hidden } from '@material-ui/core';

function GuardRoute() {
    return function ({ component: Component, ...rest }) {
        return (
            <Route
                {...rest}
                render={(props: JSX.IntrinsicAttributes) => (getCookie('authorization') !== undefined ? <Component {...props} /> : <Redirect to="/login" />)}
            />
        );
    };
}

class Dashboard extends React.Component<{}, {}> {

    render() {
        const PrivateRoute = GuardRoute();

        return (
            <Router>
                <Hidden xsDown>
                    <LargeNav />
                </Hidden>
                <Hidden smUp>
                    <TopSmallNav />
                </Hidden>

                <Switch>
                    <PrivateRoute path='/dashboard/trackersFeed' component={TrackersFeed} />
                    <PrivateRoute path='/dashboard/singleGlobeTrotter' component={SingleGlobeTrotter} />
                    <PrivateRoute path='/dashboard/postPost' component={PostPost} />
                    <PrivateRoute path='/dashboard/account' component={Account} />
                </Switch>

                <Hidden smUp>
                    <BottomSmallNav />
                </Hidden>

            </Router>

        );
    }
}
export default Dashboard;

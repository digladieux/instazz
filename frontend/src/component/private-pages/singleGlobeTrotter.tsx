import React from 'react';
import GlobeTrotterModel from '../models/globeTrotterModel';
import GlobeTrotterService from '../services/globeTrotterService';
import { ShowLoader, ErrorAlert, CloseLoader } from '../alert';
import { redirection } from '../globalFunctions';
import PostModel from '../models/postModel';

import queryString from 'query-string';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import { Tooltip } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';

import "./../../assets/style/singleGlobeTrotter.css"
import SidebarAccount from './sidebar/sidebarAccount';

import SpeakerNotesIcon from '@material-ui/icons/SpeakerNotes';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';

import PostService from '../services/postService';

import Dialog from "@material-ui/core/Dialog";
import Zoom from "@material-ui/core/Zoom";
import { TransitionProps } from "@material-ui/core/transitions";
import SinglePost from './singlePost';
import { getCookie } from "../cookies";


type State = {
    id: string
    globe_trotter: GlobeTrotterModel,
    posts: PostModel[],
    open: boolean,
    selectedPost: PostModel | null,
    selectedGlobetrotter: GlobeTrotterModel | null,
};

const Transition = React.forwardRef<unknown, TransitionProps>(
    function Transition(props, ref) {
        return <Zoom ref={ref} {...props} />;
    }
);

class SingleGlobeTrotter extends React.Component<{}, State>  {

    private globeTrotterService: GlobeTrotterService = new GlobeTrotterService();
    private postService: PostService = new PostService();
    constructor(props: any) {
        super(props);
        const url: string = props.location?.search;
        const params: any = queryString.parse(url);
        this.state = {
            id: params.id,
            globe_trotter: new GlobeTrotterModel(null),
            posts: [],
            open: false,
            selectedPost: null,
            selectedGlobetrotter: null,
        }
    }
    async componentDidMount() {

        ShowLoader();
        let result_globe_trotter: any;
        let id;
        if (this.state.id) {
            id = this.state.id;
            result_globe_trotter = await this.globeTrotterService.getGlobeTrotterById(this.state.id);

        } else {
            id = getCookie('id');
            result_globe_trotter = await this.globeTrotterService.getMe();
        }
        if (result_globe_trotter.status === 200) {
            const result_posts = await this.postService.getPostsGlobeTrotter(id);
            if (result_posts.status === 200) {
                this.setState({ globe_trotter: result_globe_trotter.data } as Pick<State, keyof State>);
                this.setState({ posts: result_posts.data } as Pick<State, keyof State>);
                CloseLoader();
            } else {
                ErrorAlert(result_posts.response.data).then();
                redirection(this.props, '/login');
            }
        } else {
            ErrorAlert(result_globe_trotter.response.data).then();
            redirection(this.props, '/login');
        }
    }

    toggleDialog(open: boolean, post: PostModel | null, globe_trotter: GlobeTrotterModel | null) {
        return () => this.setState({ open, selectedPost: post, selectedGlobetrotter: globe_trotter });
    }

    PostList() {
        let listPost: Array<any> = new Array<any>();
        if (this.state.globe_trotter?.posts) {
            for (let i = 0; i < this.state.globe_trotter.posts.length; i++) {
                listPost.push(this.displayPostAccount(this.state.posts[i], this.state.globe_trotter, i));
            }
        }

        return (
            <Grid container>
                {listPost}
            </Grid>

        );
    }

    displayPostAccount(post: PostModel, globe_trotter: GlobeTrotterModel, i: number) {
        return (
            < Grid key={i} item xs={6} sm={6} md={6} className="post_container" >
                <Card className="no-sidebar-post-card" >
                    <CardMedia
                        className="media"
                        image={post?.pictures[0]}
                        title="User Photo"
                        onClick={this.toggleDialog(true, post, globe_trotter)}
                    />
                </Card>
            </Grid >
        );
    }

    render() {
        const { open } = this.state;

        var username = '';
        var profile_picture = '';
        var last_name = '';
        var first_name = '';
        var biography = '';
        var posts;
        var statistic_followers;
        var statistic_followed;
        var id = '';
        if (this.state.globe_trotter) {
            id = String(this.state.globe_trotter.id);
            username = String(this.state.globe_trotter.username);
            profile_picture = String(this.state.globe_trotter.profile_picture);
            last_name = String(this.state.globe_trotter.last_name);
            first_name = String(this.state.globe_trotter.first_name);
            biography = String(this.state.globe_trotter.biography);
            posts = this.state.globe_trotter?.posts;
            statistic_followers = this.state.globe_trotter?.statistic_followers;
            statistic_followed = this.state.globe_trotter?.statistic_followed;
        }

        return (
            <>
                <Grid container className="user-account-container" >
                    <Grid item sm={5} md={3} >
                        <Box className="sidebar" display={{ xs: 'none', sm: 'block' }}>
                            <SidebarAccount globe_trotter={this.state.globe_trotter} />
                        </Box>
                    </Grid>
                    <Grid item xs={12} sm={7} md={9} className="user-posts">
                        <Box className="no-sidebar" display={{ xs: 'flex', sm: 'none' }}>
                            <Grid item xs={4} className="no-sidebar-user-photo">
                                <div className="account-photo">
                                    <img src={profile_picture} alt="profil" />
                                </div>
                            </Grid>
                            <Grid item xs={8} className="no-sidebar-user-info">
                                <div className="no-sidebar-user-pseudo-and-name">
                                    {username} ({first_name} {last_name})
                            </div>

                                <div className="user-biography-style">
                                    <Tooltip title={biography}>
                                        <div className="user-biography-text">
                                            {biography}<br></br>
                                        </div>
                                    </Tooltip>
                                </div>

                                <div className="no-sidebar-user-stats">
                                    <div>{posts ? posts.length : 0} <SpeakerNotesIcon fontSize="small" /></div>
                                    <div>{statistic_followers ? statistic_followers.length : 0} <PeopleAltIcon fontSize="small" /></div>
                                    <div>{statistic_followed ? statistic_followed.length : 0} <VisibilityIcon fontSize="small" /></div>
                                </div>

                                {getCookie('id') !== id ?
                                    <button className="follow" onClick={() => { ShowLoader(); this.globeTrotterService.postLike(id); CloseLoader() }}>
                                        {statistic_followed !== undefined && statistic_followed.length !== 0 && statistic_followed.find((id_stat: string) => id_stat === this.state.id) ? <div className="gray">Ne plus suivre</div> : 'Suivre'}
                                    </button>
                                    : ''}
                            </Grid>


                        </Box>
                        <Box className="no-sidebar-post-list" display={{ xs: 'grid', sm: 'none' }}>
                            {this.PostList()}
                        </Box>
                        <Box className="with-sidebar-post-list" display={{ xs: 'none', sm: 'grid' }}>
                            {this.PostList()}
                        </Box>

                    </Grid>
                </Grid>
                <Dialog
                    open={open}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={this.toggleDialog(false, null, null)}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    {(this.state.selectedPost && this.state.selectedGlobetrotter) && (
                        <SinglePost post={this.state.selectedPost} globe_trotter={this.state.selectedGlobetrotter} />
                    )}
                </Dialog>
            </>
        );
    }
}

export default SingleGlobeTrotter;
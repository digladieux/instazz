import React from 'react';
import PostModel from '../models/postModel';
import PostService from '../services/postService';
import CommentService from '../services/commentService';
import GlobeTrotterService from '../services/globeTrotterService';
import { displayTimeDifference } from '../globalFunctions'


import { ShowLoader, CloseLoader, AlertInput } from '../alert';

import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ChatIcon from '@material-ui/icons/Chat';

import "./../../assets/style/trackerFeed.css";

import GlobeTrotterModel from '../models/globeTrotterModel';
import SidebarTrackersFeed from './sidebar/sidebarTrackersFeed'
import CommentModel from '../models/commentModel';

import Dialog from "@material-ui/core/Dialog";
import Zoom from "@material-ui/core/Zoom";
import { TransitionProps } from "@material-ui/core/transitions";
import SinglePost from './singlePost';
import { redirection } from '../globalFunctions';
import { getCookie } from '../cookies';


type state = {
    page_number: number,
    page_size: number,
    posts: PostModel[],
    globe_trotters: GlobeTrotterModel[],
    open: boolean,
    selectedPost: PostModel | null,
    selectedGlobetrotter: GlobeTrotterModel | null,
    id: string
};

const Transition = React.forwardRef<unknown, TransitionProps>(
    function Transition(props, ref) {
        return <Zoom ref={ref} {...props} />;
    }
);

class TrackersFeed extends React.Component<{}, state>  {

    private postService: PostService = new PostService();
    private commentService: CommentService = new CommentService();
    private globeTrotterService: GlobeTrotterService = new GlobeTrotterService();

    constructor(props: any) {
        super(props);
        this.state = {
            page_number: -1,
            page_size: 6,
            posts: [],
            globe_trotters: [],
            open: false,
            selectedPost: null,
            selectedGlobetrotter: null,
            id: getCookie('id')
        }
        this.getPosts = this.getPosts.bind(this);
    }

    toggleDialog(open: boolean, post: PostModel | null, globe_trotter: GlobeTrotterModel | null) {
        return () => this.setState({ open, selectedPost: post, selectedGlobetrotter: globe_trotter });
    }

    async componentDidMount() {
        await this.getPosts();
    }

    async postLike(post: PostModel) {
        ShowLoader();
        const res = await this.postService.postLike(post._id);

        if (res.status === 201) {
            post.statistic = res.data;
        }
        this.setState({ posts: this.state.posts });
        CloseLoader();
    }

    async getPosts() {
        ShowLoader();
        const result = await this.postService.getPostsBySize(this.state.page_number + 1, this.state.page_size);
        if (result.status === 200) {
            const posts = this.state.posts;
            const globe_trotters = this.state.globe_trotters;

            for (let i = 0; i < result.data.posts.length; i++) {
                posts.push(result.data.posts[i]);
                globe_trotters.push(result.data.globe_trotters[i]);
            }

            this.setState({ posts: posts });
            this.setState({ globe_trotters: globe_trotters });
            this.setState({ page_number: this.state.page_number + 1 });



        }
        CloseLoader();
    }

    async postComment(post: PostModel) {
        AlertInput().then(async (text: any) => {
            const data = {
                description: text.value,
                id_post: post._id
            };
            ShowLoader();
            const res = await this.commentService.postComment(data);

            if (res.status === 201) {
                post = res.data;
            }
            this.setState({ posts: this.state.posts });
            CloseLoader();
            redirection(this.props, '/dashboard/trackersFeed');
        });
    }

    displayPostTrackersFeed(post: PostModel, globe_trotter: GlobeTrotterModel) {

        return (
            < Grid item xs={12} sm={12} md={6} className="post_container" key={post._id} >
                <Card className="post_card" >
                    <CardMedia
                        className="media"
                        image={post.pictures[0]}
                        title="User Photo"
                        onClick={this.toggleDialog(true, post, globe_trotter)}
                    />
                    <CardHeader
                        title={
                            <div className="post_location_and_flag"> <img className="flag" src={"/flags/" + String(post.country).toLowerCase() + ".svg"} alt="svg" />
                                <div className="post_location_date">{post.location}, le {post.shooting_date}</div>
                            </div>}
                    />
                    <div className="post_general_ingo">
                        <CardHeader
                            avatar={
                                <Avatar alt={String(globe_trotter?.username)} src={String(globe_trotter?.profile_picture)}>

                                </Avatar>
                            }
                            action={<div className="ago">{displayTimeDifference(post.publication_date)}</div>}
                            title={globe_trotter?.username}
                        />


                        <CardContent  >
                            <Typography variant="h5" component="p" className="post_title">
                                {post.title}
                            </Typography>
                            <Typography variant="body2" component="p" className="post_description">
                                {post.description}
                            </Typography>
                        </CardContent>
                    </div>
                    <CardActions >
                        <IconButton aria-label="J'aime" onClick={() => this.postLike(post)} >
                            {post?.statistic?.length} {post?.statistic?.find((id_stat: string) => id_stat === this.state.id) ? <FavoriteIcon color="secondary" /> : <FavoriteIcon />}
                        </IconButton>
                        <IconButton aria-label="Commenter" onClick={() => this.postComment(post)}>
                            {post.comments.length} <ChatIcon />
                        </IconButton>
                    </CardActions>
                </Card>
            </Grid >
        );
    }

    PostList() {
        let listPost: Array<any> = new Array<any>();
        for (let i = 0; i < this.state.posts.length; i++) {
            listPost.push(this.displayPostTrackersFeed(this.state.posts[i], this.state.globe_trotters[i]));
        }

        return (
            <Grid container>
                {listPost}
            </Grid>

        );
    }

    CommentList(post: PostModel) {
        let listComment: Array<any> = new Array<any>();
        for (let i = 0; i < post.comments.length; i++) {
            listComment.push(this.displayComment(post.comments[i]));
        }

        return (
            <>
                {listComment}
            </>
        );
    }

    async displayComment(comment: CommentModel) {
        const comment_globe_trotter: GlobeTrotterModel = await this.globeTrotterService.getGlobeTrotterById(String(comment.id_globe_trotter));
        return (
            <div className="comment_container">
                <div className="comment_header">
                    <div className="comment_user">
                        <img src={String(comment_globe_trotter.profile_picture)} alt="comment_avatar" className="comment_avatar" />
                        {comment_globe_trotter.username}
                    </div>
                    <div className="comment_ago">
                        {displayTimeDifference(comment.publication_date)}
                    </div>
                </div>
                <div className="comment_body">
                    {comment.description}
                </div>
            </div>
        );
    }

    render() {
        const { open } = this.state;

        return (
            <>
                <Grid container className="trackerFeed-sidebar-container" >
                    <Grid item sm={5} md={3} >
                        <Box className="sideBar" display={{ xs: 'none', sm: 'block' }}>
                            <SidebarTrackersFeed />
                        </Box>
                        <button type="button" className="seeMorePosts" onClick={this.getPosts}>Afficher plus de publication</button>

                    </Grid>
                    <Grid item xs={12} sm={7} md={9} className="trackerFeed">

                        {this.PostList()}

                    </Grid>

                </Grid>
                {open ? <Dialog
                    open={open}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={this.toggleDialog(false, null, null)}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    {(this.state.selectedPost && this.state.selectedGlobetrotter) && (
                        <SinglePost post={this.state.selectedPost} globe_trotter={this.state.selectedGlobetrotter} />
                    )}
                </Dialog> : ''}
            </>

        );
    }
}


export default TrackersFeed;
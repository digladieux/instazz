import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';

class Notifications extends React.Component {

    render() {
        return (
            <div className="notification_container">
                <Card className="user-item">
                    <CardHeader
                        className="user-header"
                        avatar={
                            <Avatar alt="Travis Howard" src="">
                            </Avatar>
                        }
                        title="Coming"
                        subheader="really soon ..."
                    />
                </Card>
            </div>
        )
    }


}

export default Notifications;
import FavoriteIcon from '@material-ui/icons/Favorite';
import ChatIcon from '@material-ui/icons/Chat';
import CardActions from '@material-ui/core/CardActions';
import React from "react";
import CommentModel from "../models/commentModel";
import PostModel from "../models/postModel";
import CommentService from '../services/commentService';
import { ShowLoader, CloseLoader, AlertInput } from '../alert';
import { displayTimeDifference } from '../globalFunctions'
import Card from '@material-ui/core/Card';
import IconButton from '@material-ui/core/IconButton';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import GlobeTrotterModel from "../models/globeTrotterModel";
import GlobeTrotterService from "../services/globeTrotterService";
import PostService from '../services/postService';
import { getCookie } from '../cookies';

function displayComment(globeTrotter: GlobeTrotterModel, comment: CommentModel) {

    let username = '';
    let profil_picture = '';
    if (globeTrotter != null) {
        username = globeTrotter.username;
        profil_picture = globeTrotter.profile_picture;
    }
    return (
        <div key={String(comment._id)} className="comment_container">
            <div className="comment_header">
                <div className="comment_user">
                    <img src={profil_picture} alt="profil_picture" />
                    {username}
                </div>
                <div className="comment_ago">
                    {displayTimeDifference(comment.publication_date)}
                </div>
            </div>
            <div className="comment_body">
                {comment.description}
            </div>
        </div>
    )
}

type State = { globeTrotters: Array<GlobeTrotterModel>, id: string }
type Props = { post: PostModel, globe_trotter: GlobeTrotterModel }
class SinglePost extends React.Component<Props, State>  {

    private _comment_service: CommentService = new CommentService();
    private globeTrotterService: GlobeTrotterService = new GlobeTrotterService();
    private postService: PostService = new PostService();

    constructor(props) {
        super(props);
        this.state = {
            globeTrotters: [],
            id: getCookie('id')

        }
    }
    async postComment(id_post: String) {
        AlertInput().then(async (text: any) => {
            const data = {
                description: text.value,
                id_post: id_post
            };
            ShowLoader();
            await this._comment_service.postComment(data);
            CloseLoader();
        });
    }

    async componentDidMount() {

        let globeTrotterAsync: Array<GlobeTrotterModel> = [];
        await Promise.all(this.props.post.comments.map(async (comment: CommentModel) => {

            if (comment.id_globe_trotter) {
                const result: any = await this.globeTrotterService.getGlobeTrotterById(String(comment.id_globe_trotter));
                if (result.status === 200) {
                    globeTrotterAsync.push(new GlobeTrotterModel(result.data));
                }
                else {
                    globeTrotterAsync.push(new GlobeTrotterModel(null));
                }
            }
            else {
                globeTrotterAsync.push(new GlobeTrotterModel(null));

            }
        }));

        this.setState({ globeTrotters: globeTrotterAsync } as Pick<State, keyof State>);

    }


    render() {
        var commentList: Array<any> = [];
        if (this.props.post.comments && this.props.post.comments.length > 0 && this.state.globeTrotters.length > 0) {
            for (let i = 0; i < this.props.post.comments.length; i++) {
                const comment = displayComment(this.state.globeTrotters[i], this.props.post.comments[i]);
                commentList.push(comment);

            }
        }

        return (
            <Card className="single_post_card" >
                <CardMedia
                    className="single_post_media"
                    image={this.props.post.pictures[0]}
                    title="User Photo"
                />
                <CardHeader
                    title={
                        <div className="post_location_and_flag">
                            <img className="flag" src={"/flags/" + String(this.props.post?.country).toLowerCase() + ".svg"} alt="svg" />
                            <div className="post_location_date">{this.props.post?.location}, le {this.props.post?.shooting_date}</div>
                        </div>}
                />
                <div className="post_general_ingo">
                    <CardHeader
                        avatar={
                            <Avatar alt={String(this.props.globe_trotter?.username)} src={String(this.props.globe_trotter?.profile_picture)}>
                            </Avatar>
                        }
                        action={<div className="ago">{displayTimeDifference(this.props.post.publication_date)}</div>}
                        title={this.props.globe_trotter?.username}
                    />
                    <CardContent  >
                        <Typography variant="h5" component="p" className="post_title">
                            {this.props.post.title}
                        </Typography>
                        <Typography variant="body2" component="p" className="post_description">
                            {this.props.post.description}
                        </Typography>
                    </CardContent>
                </div>
                <CardActions >
                    <IconButton aria-label="J'aime" onClick={() => { ShowLoader(); this.postService.postLike(this.props.post._id); CloseLoader() }} >
                        {this.props.post.statistic.length} {this.props.post?.statistic?.find((id_stat: string) => id_stat === this.state.id) ? <FavoriteIcon color="secondary" /> : <FavoriteIcon />}                    </IconButton>
                    <IconButton aria-label="Commenter" onClick={() => this.postComment(String(this.props.post._id))}>
                        {this.props.post.comments.length} <ChatIcon />
                    </IconButton>

                </CardActions>

                <div className="comment_list_title">
                    Liste des commentaires:
                </div>
                <div className="comment_list">
                    {commentList}
                </div>
            </Card >
        );
    }
}

export default SinglePost;
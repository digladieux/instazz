import PutGlobeTrotterForm from '../form/putGlobeTrotterForm'
import React from 'react'
import Grid from '@material-ui/core/Grid';

import "./../../assets/style/register.css"
import { ShowLoader, CloseLoader, ErrorAlert } from '../alert';
import { getCookie } from '../cookies';
import GlobeTrotterService from '../services/globeTrotterService';
import { redirection } from '../globalFunctions';
import GlobeTrotterModel from '../models/globeTrotterModel';

type State = {
    globeTrotter: GlobeTrotterModel
}
class Account extends React.Component<{}, State> {

    private globeTrotterService: GlobeTrotterService = new GlobeTrotterService();

    constructor(props) {
        super(props);
        this.state = {
            globeTrotter: new GlobeTrotterModel(null)
        }
    }
    async componentDidMount() {
        ShowLoader();
        let result: any;
        if (getCookie('authorization') !== undefined) {
            result = await this.globeTrotterService.getMe();
        }
        if (result.status === 200) {
            this.setState({ globeTrotter: new GlobeTrotterModel(result.data) } as Pick<State, keyof State>);
            CloseLoader();
        } else {
            ErrorAlert(result.response.data).then();
            redirection(this.props, '/login');
        }
    }

    render() {
        return (
            <div className="general-form-container">

                <Grid container>
                    <Grid item md={2} sm={1}>
                    </Grid>
                    <Grid item md={8} sm={10}>
                        <div className="form-container">
                            <div className="form-header"><h3>Modifier mes informations</h3></div>
                            <div className="form">
                                <PutGlobeTrotterForm globeTrotter={this.state.globeTrotter} />
                            </div>
                        </div>
                    </Grid>
                    <Grid item md={2} sm={1}>
                    </Grid>
                </Grid>
            </div>

        );
    }
}

export default Account
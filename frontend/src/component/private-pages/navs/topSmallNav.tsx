import React from "react";
import AppBar from "@material-ui/core/AppBar"; //Privilégier les imports individuels

import { Toolbar } from "@material-ui/core"; //plutôt que ça

import '../../../assets/style/nav.css';

class TopSmallNav extends React.Component<{}, {}>  {

  render() {

    return (
      <>
        <AppBar position="static" className="top_small_nav">
          <Toolbar>


            <ul className="nav__menu push">
              {/* <li>
                <Tooltip title="Accéder aux paramètres" arrow>
                  <Link to={'/account'} className="nav-link push"><SettingsIcon color="action" /></Link>
                </Tooltip>
              </li> */}
            </ul>

          </Toolbar>
        </AppBar>
      </>
    );
  }
}

export default TopSmallNav;
import React from "react";
import AppBar from "@material-ui/core/AppBar"; //Privilégier les imports individuels
import { Link } from 'react-router-dom';
import { Tooltip } from '@material-ui/core';
import { withRouter } from 'react-router-dom'

import { Toolbar } from "@material-ui/core"; //plutôt que ça
import HomeIcon from '@material-ui/icons/Home';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import NotificationsIcon from '@material-ui/icons/Notifications';
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import IconButton from '@material-ui/core/IconButton';

import { withStyles, Theme } from '@material-ui/core/styles';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import '../../../assets/style/nav.css';
import Notifications from "../notification";

const HtmlTooltip = withStyles((theme: Theme) => ({
  tooltip: {
    backgroundColor: '#C4C4C4',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 450,
    maxHeight: 500,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    borderRadius: 5
  },
}))(Tooltip);

type state = {
  isTooltipOpen: boolean
};

class LargeNav extends React.Component<{}, state>  {
  constructor(props: any) {
    super(props);
    this.state = {
      isTooltipOpen: false,
    }
  }

  handleTooltipClose() {
    return () => this.setState({ isTooltipOpen: false });

  }

  handleTooltipOpen() {
    return () => this.setState({ isTooltipOpen: true });

  }

  render() {

    return (
      <>
        <AppBar position="static" className="large_nav ">
          <Toolbar>
            <ul className="nav__menu ">
              <li>

                <IconButton>
                  <Tooltip title="Accueil" arrow>
                    <Link to={'/dashboard/trackersFeed'} className="nav-link"> <HomeIcon fontSize="large" /></Link>
                  </Tooltip>

                </IconButton>
              </li>
              <li>
                <IconButton>
                  <Tooltip title="Créer une nouvelle publication" arrow>
                    <Link to={'/dashboard/postPost'} className="nav-link"><AddAPhotoIcon fontSize="large" /></Link>
                  </Tooltip>
                </IconButton>
              </li>
            </ul>

            <ul className="nav__menu push">
              <li>
                <IconButton>
                  <Tooltip title="Voir votre compte" arrow>
                    <Link to={'/dashboard/singleGlobeTrotter'} className="nav-link push"><AccountCircleIcon fontSize="large" /></Link>
                  </Tooltip>
                </IconButton>
              </li>
              <li>
                <IconButton>
                  <ClickAwayListener onClickAway={this.handleTooltipClose()}>
                    <div>
                      <HtmlTooltip
                        interactive
                        placement="bottom-end"
                        PopperProps={{
                          disablePortal: true,
                        }}
                        onClose={this.handleTooltipClose()}
                        open={this.state.isTooltipOpen}
                        disableFocusListener
                        disableHoverListener
                        disableTouchListener
                        title={
                          <React.Fragment>
                            <Notifications />
                          </React.Fragment>
                        }
                      >
                        <div className="nav-link push" onClick={this.handleTooltipOpen()}><NotificationsIcon fontSize="large" /></div>
                      </HtmlTooltip>
                    </div>
                  </ClickAwayListener>
                </IconButton>

              </li>
              {/* <li>
                <Tooltip title="Accèder aux paramètres" arrow>
                  <IconButton><Link to={'/dashboard/account'} className="nav-link push" ><SettingsIcon fontSize="large" /></Link></IconButton>
                </Tooltip>
              </li> */}
            </ul>

          </Toolbar>
        </AppBar>
      </>
    );
  }
}

export default withRouter(LargeNav);
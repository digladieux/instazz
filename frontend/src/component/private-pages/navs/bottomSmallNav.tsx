import React from "react";
import AppBar from "@material-ui/core/AppBar"; //Privilégier les imports individuels
import { Link } from 'react-router-dom';
import { Tooltip } from '@material-ui/core';

import { Toolbar } from "@material-ui/core"; //plutôt que ça
import HomeIcon from '@material-ui/icons/Home';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import NotificationsIcon from '@material-ui/icons/Notifications';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircleIcon from '@material-ui/icons/AccountCircle'

import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import SidebarTrackersFeed from "../sidebar/sidebarTrackersFeed";

import '../../../assets/style/nav.css';
import Notifications from "../notification";

type state = {
  isOpenNotification: boolean,
  isOpenSearch: boolean
};


class BottomSmallNav extends React.Component<{}, state>  {

  constructor(props: any) {
    super(props);
    this.state = {
      isOpenNotification: false,
      isOpenSearch: false

    }
  }

  toggleDrawerNotification(isOpenNotification) {
    return () => this.setState({ isOpenNotification });
  }

  toggleDrawerSearch(isOpenSearch) {
    return () => this.setState({ isOpenSearch });
  }

  render() {
    const { isOpenNotification } = this.state;
    const { isOpenSearch } = this.state;


    return (
      <>
        {/** Voir React.Fragment pour les balises vides, ça évite les divs inutiles */}
        <AppBar position="static" className="bottom_small_nav">
          <Toolbar>
            <ul className="nav__menu small">
              <li>
                <Tooltip title="Accueil" arrow>
                  <Link to={'/dashboard/trackersFeed'} className="nav-link"> <HomeIcon /></Link>
                </Tooltip>
              </li>
              <li onClick={this.toggleDrawerSearch(true)} >
                <Tooltip title="Rechercher" arrow>
                  <div className="nav-link"> <SearchIcon /></div>
                </Tooltip>
              </li>
              <li>
                <Tooltip title="Créer une nouvelle publication" arrow>
                  <Link to={'/dashboard/postPost'} className="nav-link"><AddAPhotoIcon /></Link>
                </Tooltip>
              </li>

              <li onClick={this.toggleDrawerNotification(true)}>
                <Tooltip title="Voir vos notifications" arrow>
                  <div className="nav-link"><NotificationsIcon /></div>
                </Tooltip>
              </li>
              <li>
                <Tooltip title="Voir votre compte" arrow>
                  <Link to={'/dashboard/singleGlobeTrotter'} className="nav-link"><AccountCircleIcon /></Link>
                </Tooltip>
              </li>
            </ul>

          </Toolbar>
        </AppBar>

        <SwipeableDrawer
          anchor="right"
          open={isOpenNotification}
          onClose={this.toggleDrawerNotification(false)}
          onOpen={this.toggleDrawerNotification(true)}
        >
          <h5>Vos notifications:</h5>
          <Notifications />
        </SwipeableDrawer>

        <SwipeableDrawer
          anchor="left"
          open={isOpenSearch}
          onClose={this.toggleDrawerSearch(false)}
          onOpen={this.toggleDrawerSearch(true)}
        >

          <SidebarTrackersFeed />
        </SwipeableDrawer>
      </>
    );
  }
}

export default BottomSmallNav;
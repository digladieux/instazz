import React from 'react';
import "./../../assets/style/template.css";

type state = {};
class Template extends React.Component<{}, state>  {

    constructor(props: any) {
        super(props);
        this.state = {}
    }

    render() {

        return (
            <div className="template-mail-container">
                <div className="header-mail">

                </div>

                <div className="content-mail-container">
                    <div className="content-mail">
                        <div className="title-mail">

                        </div>
                        <div className="message-mail">

                        </div>
                    </div>

                    <button className="button-mail">

                    </button>
                </div>
            </div>
        );
    }
}

export default Template;
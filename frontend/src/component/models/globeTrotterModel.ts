import PostModel from "./postModel";

class GlobeTrotterModel {
    public _id: string;
    private _username: string;
    private _email: string;
    private _first_name: string;
    private _last_name: string;
    private _birth_date: string;
    private _biography: string;
    public _profile_picture: string;
    private _trackers: Array<string>;
    private _country_localisation: string;
    private _posts: Array<PostModel>;
    private _statistic_followers: Array<string>;
    private _statistic_followed: Array<string>;

    constructor(data: any) {
        this._id = data?._id;
        this._username = data?.username;
        this._email = data?.email;
        this._first_name = data?.first_name;
        this._last_name = data?.last_name;
        this._birth_date = data?.age;
        this._biography = data?.biography;
        this._trackers = data?.trackers;
        this._country_localisation = data?.country_localisation;
        this._profile_picture = data?.profile_picture;
        this._posts = data?.posts;
        this._statistic_followers = data?.statistic_followes;
        this._statistic_followed = data?.statistic_followed;
    }

    get id(): string {
        return this._id;
    }

    get username(): string {
        return this._username;
    }

    get email(): string {
        return this._email;
    }

    get first_name(): string {
        return this._first_name;
    }

    get last_name(): string {
        return this._last_name;
    }

    get birth_date(): string {
        return this._birth_date;
    }

    get biography(): string {
        return this._biography;
    }

    get profile_picture(): string {
        return this._profile_picture;
    }

    get trackers(): Array<string> {
        return this._trackers;
    }

    get country_localisation(): string {
        return this._country_localisation;
    }

    get posts(): Array<PostModel> {
        return this._posts;
    }

    get statistic_followers(): Array<string> {
        return this._statistic_followers;
    }

    get statistic_followed(): Array<string> {
        return this._statistic_followed;
    }
}

export default GlobeTrotterModel
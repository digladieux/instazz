import Validations from "./validations";

class AlphabetValidation extends Validations {
    constructor(value: string) {
        super(value);
        this.required();
        this.onlyAlphabetCharacters()
    }
}

export default AlphabetValidation;
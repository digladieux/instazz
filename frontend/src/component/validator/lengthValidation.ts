import Validations from "./validations";

class LengthValidation extends Validations {
    constructor(value: string, length: number) {
        super(value);
        this.required();
        this.length(length);
    }
}

export default LengthValidation;
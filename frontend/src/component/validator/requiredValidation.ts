import Validations from "./validations";

class RequiredValidation extends Validations {

    constructor(value: string) {
        super(value);
        this.required();
    }
}

export default RequiredValidation;
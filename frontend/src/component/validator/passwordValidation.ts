import Validations from "./validations";

class PasswordValidator extends Validations {

    constructor(value: string) {
        super(value);
        this.required();
        this.length(8);
        this.containLowercase();
        this.containUppercase();
        this.containNumeric();
        this.containSpecialCharacter();
    }
}

export default PasswordValidator;
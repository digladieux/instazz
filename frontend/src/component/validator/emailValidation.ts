import Validations from "./validations";

class EmailValidation extends Validations {
    constructor(value: string) {
        super(value);
        this.required();
        this.isEmailValid()
    }
}

export default EmailValidation;
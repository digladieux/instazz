import Validations from "./validations";

class EqualValueValidation extends Validations {
    constructor(value: string, equal_value: string) {
        super(value);
        this.equalValue(equal_value);
    }
}

export default EqualValueValidation;
class Validations {

    private _valueToValidate: any;
    protected _errors: Map<string, string> = new Map<string, string>();

    constructor(valueToValidate: any) {
        this._valueToValidate = valueToValidate;
    }

    range(min: number, max: number) {
        if (this._valueToValidate < min || this._valueToValidate > max) {
            this._errors.set('range', "La valeur doit être contenue entre " + min + " et " + max);
        }
    }

    length(value: Number) {
        if (this._valueToValidate.length < value) {
            this._errors.set('length', "Ce champs doit contenir au moins " + value + " charactères");
        }
    }

    required() {
        if (this._valueToValidate.trim() === "" || this._valueToValidate.trim().length === 0) {
            this._errors.set('required', "Ce champs doit être renseigné");
        }
    }

    equalValue(value: String) {
        if (this._valueToValidate !== value) {
            this._errors.set('equal', "Ces champs doivent être identiques");
        }
    }
    containLowercase() {
        var lowercase_regex = new RegExp("[a-z]");
        if (!this._valueToValidate.match(lowercase_regex)) {
            this._errors.set('lowercase', 'Ce champs doit contenir au moins une lettre minuscule');
        }
    }

    containUppercase() {
        var uppercase_regex = new RegExp("[a-z]");
        if (!this._valueToValidate.match(uppercase_regex)) {
            this._errors.set('uppercase', 'Ce champs doit contenir au moins une lettre majuscule');
        }
    }

    containNumeric() {
        var numeric_regex = new RegExp("[0-9]");
        if (!this._valueToValidate.match(numeric_regex)) {
            this._errors.set('numeric', 'Ce champs doit contenir au minimum un chiffre');
        }
    }

    containSpecialCharacter() {
        var special_regex = new RegExp("[0-9]");
        if (!this._valueToValidate.match(special_regex)) {
            this._errors.set('special', 'Ce champs doit contenir au minimum un charatère special comme !@#$%^&');
        }
    }

    isEmailValid() {

    }

    onlyAlphabetCharacters() {
        var special_regex = new RegExp("^[a-zA-Z]+$");
        if (!this._valueToValidate.match(special_regex)) {
            this._errors.set('special', 'Ce champs ne doit contenir que des lettres');
        }
    }

    errors() {
        return this._errors;
    }

    isValid() {
        return this._errors.size === 0;
    }
}

export default Validations;
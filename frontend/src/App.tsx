import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Register from './component/public-pages/register';
import Login from './component/public-pages/login';
import RecoveryPassword from './component/public-pages/recoveryPassword';
import ChangePassword from './component/public-pages/changePassword';
import Callback from './component/public-pages/callback';
import Dashboard from './component/private-pages/dashboard';
import NotFound from './component/public-pages/notFound';

class App extends React.Component<{}, {}> {
  render() {

    return (
      <Router>
        <Switch>
          <Route exact path='/' component={Login} />
          <Route path='/dashboard' component={Dashboard} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/register' component={Register} />
          <Route exact path='/recoveryPassword' component={RecoveryPassword} />
          <Route exact path='/changePassword' component={ChangePassword} />
          <Route exact path='/callback' component={Callback} />
          <Route path='*' component={NotFound} />
        </Switch>
      </Router>

    );
  }
}
export default App;
